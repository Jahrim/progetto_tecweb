<?php
require_once 'init.php';

if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['confirmPassword'])) {
    if ($_POST['password'] != $_POST['confirmPassword']) {
        $tp["signin_error"]["password_dont_match"] = 'Errore! Le due password inserite non corrispondono.';
    } else {
        if ($databaseHelper->isEmailAvailable($_POST['email'])) {
            if($databaseHelper->registerUser($_POST['name'],$_POST['email'],$_POST['password']) == 0) {
                $tp["signin_success"] = 'Registrazione avvenuta con successo!';
                $userID = $databaseHelper->getUserByEmail($_POST['email'])['userID'];
                $databaseHelper->insertNotification($userID, 'Benvenuto!', 'Grazie per aver scelto ShopZone!');
                require 'login.php';
            } else {
                $tp["signin_error"]["registration_failed"] = 'Errore! Il server non è riuscito a memorizzare i tuoi dati.';
            }
        } else {
            $tp["signin_error"]["email_already_taken"] = 'Errore! L\'email inserita è già registrata.';
        }
    }
}

if (!isset($tp['signin_success'])){
    if (!UserHelper::isLoggedIn()){
        /* IMPORT ---------------------------------------------------------------------------*/
        $tp["css"] = array(
            "external/bootstrap-v4.5.3.css",
            "css/components/comp-header-footer.css",
            "css/pages/page-signin.css"
        );
        $tp["icon"] = array(
            "https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css"
        );
        $tp["js"] = array(
            "external/jquery-v3.5.1.js",
            "external/bootstrap.bundle.v4.5.3.js"
        );

        /* PAGE DATA -------------------------------------------------------------------------*/
        $tp["meta-title"] = "Shopzone - Registrati";
        $tp["title"] = "Registrati a";
        $tp["page"] = Page::SIGNIN;

        require 'template/base.php';
    } else {
        header("location: index.php");
    }
}
