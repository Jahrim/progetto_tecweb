<?php
require_once 'init.php';

if (UserHelper::isLoggedIn()) {
    /* IMPORT ---------------------------------------------------------------------------*/
    $tp["css"] = array(
        "external/bootstrap-v4.5.3.css",
        "css/components/comp-header-footer.css",
        "css/components/comp-product-list.css",
        "css/components/comp-browse-buttons.css",
        "css/pages/page-cart.css"
    );
    $tp["icon"] = array(
        "https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css"
    );
    $tp["js"] = array(
        "external/jquery-v3.5.1.js",
        "external/bootstrap.bundle.v4.5.3.js",
        "js/util.js",
        "js/product-list.js",
        "js/browse-buttons.js",
        "js/search-bar.js",
        "js/page-cart.js"
    );

    /* PAGE DATA -------------------------------------------------------------------------*/
    $tp["meta-title"] = "Shopzone - Carrello";
    $tp["title"] = "Carrello";
    $tp["page"] = Page::CART;
    $tp["totalCartPrice"] = 0;
    if (!CartHelper::isCartEmpty()) {
        $productsInCart = $databaseHelper->getProducts(CartHelper::getCart(), $excludeCart = false);
        foreach ($productsInCart as $productInCart) {
            $tp['totalCartPrice'] += $productInCart['price'];
        }
    }
    require 'template/base.php';
} else {
    header("location: index.php");
}
