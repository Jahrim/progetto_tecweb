<?php
require_once 'init.php';

if (isset($_POST['email']) && isset($_POST['password'])) {
    $userData = $databaseHelper->getUserDataByLogin($_POST['email'], $_POST['password']);
    if (empty($userData)) {
        $tp["login_error"] = 'Errore! Controllare di aver inserito correttamente i propri dati';
    } else {
        UserHelper::saveSessionUserData($userData);
    }
}
if (!UserHelper::isLoggedIn()) {
    /* IMPORT ---------------------------------------------------------------------------*/
    $tp["css"] = array(
        "external/bootstrap-v4.5.3.css",
        "css/components/comp-header-footer.css",
        "css/pages/page-login.css"
    );
    $tp["icon"] = array(
        "https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css"
    );
    $tp["js"] = array(
        "external/jquery-v3.5.1.js",
        "external/bootstrap.bundle.v4.5.3.js"
    );

    /* PAGE DATA -------------------------------------------------------------------------*/
    $tp["meta-title"] = "Shopzone - Accedi";
    $tp["title"] = "Accedi a";
    $tp["page"] = Page::LOGIN;

    require 'template/base.php';
} else {
    header("location: index.php");
}
