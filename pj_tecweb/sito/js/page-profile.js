$(document).ready(function () {
    $('#confirm').css("display", "none");
    var $errorPassword = $("<div>").addClass("text-center text-danger");
    var $errorDealerLicense = $("<div>").text("Tutti i campi devono essere compilati.").addClass("text-center text-danger");
    var $errorMissingMail = $("<div>").text("E' necessario inserire un indirizzo email.").addClass("text-center text-danger");

    $("#updatePasswordForm").submit(function (ep) {
        ep.stopPropagation();
        if (($('#modalPassword').val() != $('#modalPasswordConfirm').val()) || $('#modalLastPassword').val() == '') {
            ep.preventDefault();
            $errorPassword.text("Le password non coincidono!");
            if ($('#modalPassword').val() == '' || $('#modalPasswordConfirm').val() == '' || $('#modalLastPassword').val() == '') {
                $errorPassword.text("Tutti i campi devono essere compilati.");
            }
            $('#errorPassword').append($errorPassword);
        }
    });

    $("#dealerLicenseForm").submit(function (el) {
        if ($('#modalIva').val() == '' || $('#modalCompanyId').val() == '' || $('#modalPec').val() == '') {
            el.preventDefault();
            $('#errorDealerLicense').append($errorDealerLicense);
        }
    });

    $("#updateDataForm").submit(function (el) {
        if ($('#email').val() == '') {
            el.preventDefault();
            $('#errorMissingEmail').append($errorMissingMail);
        }
    });

    $('#updateData').click(function (eu) {
        eu.preventDefault();
        $(this).css("display", "none");
        $('#confirm').css("display", "block");
        let $formFields = $("body .container-fluid > main fieldset:nth-child(2) input:not([type='submit'])");
        $formFields.removeAttr("disabled").addClass("form-control").removeClass("form-control-plaintext");
        $formFields.each(function () {
            $(this).val($(this).attr('placeholder'));
            $(this).removeAttr("placeholder");
        })
    });

    $('#passwordUpdateModal').on('hide.bs.modal', function (e) {
        $errorPassword.remove();
    })

    $('#dealerLicenseModal').on('hide.bs.modal', function (e) {
        $errorPassword.remove();
    })
});