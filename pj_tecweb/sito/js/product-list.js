const MAX_PRODUCT_LIST_SIZE = 15;

var currentSearch = '';
var endOfProductList = false;

const productListSelector = 'body > .container-fluid > main > div:nth-child(2)';
const productBaseSelector = productListSelector + '> article:first-of-type';
const productClonesSelector = productListSelector + '> article:not(:first-of-type)';
const productCloneAsideSelector = '.row > aside';
const productCloneImageSelector = productCloneAsideSelector + '> img';
const productCloneTitleSelector = '.row > div > header > h2';
const productCloneDescriptionSelector = '.row > div > p:nth-child(2)';
const productClonePriceSelector = '.row > div > p:nth-child(3)';

function getProductFooter(product) {
    return $('.row > div > footer > .row', product);
}
function cloneBaseProduct() {
    $(productBaseSelector).show();
    let tempProductBase = $(productBaseSelector).clone()[0];
    $(productBaseSelector).hide();
    return tempProductBase;
}

function addButtonListeners(buttonClass) {
    let buttons = $('.' + buttonClass + ' > .btn');
    if (buttons.exists()) {
        buttons.each(function (index, element) {
            let productID = ($(this).attr('id') !== undefined) ? $(this).attr('id').substring(buttonClass.length + 1) : '';
            switch (buttonClass) {
                case EnumButtonIstance.DELETE_PRODUCT:
                case EnumButtonIstance.REMOVE_FROM_CART:
                    $(this).click(function (e) {
                        e.preventDefault();
                        $.ajax({ url: $(this).attr('href') + productID });
                        $('#Product\\:' + productID).remove();
                    });
                    break;
                case EnumButtonIstance.ADD_TO_CART:
                    $(this).click(function (e) {
                        e.preventDefault();
                        $.ajax({ url: $(this).attr('href') + productID });
                        $(this).text('Nel carrello');
                        $(this).prop('disabled', true);
                    });
                    return true;
                case EnumButtonIstance.UPDATE_PRODUCT:
                case EnumButtonIstance.SHOW_PRODUCT:
                    $(this).attr({ 'href': ($(this).attr('href') + productID) });
                    return true;
                case EnumButtonIstance.RATE_PRODUCT:
                    $(this).attr({ 'href': ($(this).attr('href') + productID + '#ratingForm') });
                    return true;
                default: return false;
            }
        });
    }
}
function tryAddButtons(buttonClass, productID, productBase) {
    let button = $('.' + buttonClass + '> .btn', getProductFooter(productBase));
    if (button.exists()) { button.attr({ 'id': (buttonClass + ':' + productID) }); }
}

function getProductListHTML(productListData) {
    let productListHTML = '';
    let tempProductBase = cloneBaseProduct();
    productListData.forEach(productData => {
        $(tempProductBase).attr({ 'id': 'Product:' + productData['productID'] })
        $(productCloneAsideSelector, tempProductBase).attr({ 'aria-labelledby': productData['productID'] });
        $(productCloneImageSelector, tempProductBase).attr({
            'src': productData['image'],
            'alt': productData['name']
        });
        $(productCloneTitleSelector, tempProductBase).attr({ 'id': productData['productID'] });
        $(productCloneTitleSelector, tempProductBase).text(productData['name']);
        $(productCloneDescriptionSelector, tempProductBase).text(productData['shortDescription']);
        $(productClonePriceSelector, tempProductBase).text(productData['price'].toString().concat('€'));
        $.each(EnumButtonIstance, function (buttonIstance, buttonClass) {
            tryAddButtons(buttonClass, productData['productID'], tempProductBase);
        });
        productListHTML += tempProductBase.outerHTML;
    });
    return productListHTML;
}

function displayProducts(currentIndex = 0, searchInput = '', location = '') {
    removePreviousProducts();
    hideProductTemplate();
    if (location == '') { location = getProductLocation(); }
    let request = 'api/api-product-list.php?' + concatGET(
        queryGET(EnumQueryParameter.LIST_INDEX, currentIndex.toString()),
        queryGET(EnumQueryParameter.SEARCH_INPUT, searchInput),
        queryGET(EnumQueryParameter.PRODUCT_LOCATION, location)
    );
    return $.getJSON(request, function (data) {
        if (data.length) {
            $(document).trigger('ProductJsonReceived', false);
            let productListHTML = getProductListHTML(data);
            $(productListSelector).append(productListHTML);
            $.each(EnumButtonIstance, function (buttonIstance, buttonClass) {
                addButtonListeners(buttonClass);
            });
        } else {
            $(document).trigger('ProductJsonReceived', true);
        }
    }).done(function () { currentSearch = searchInput; });
    //.fail((jqXHR, textStatus, errorThrown) => alert(jqXHR + '   |   ' + textStatus + '   |   ' + errorThrown))
}
function removePreviousProducts() {
    $(productClonesSelector).remove();
}
function hideProductTemplate() {
    $(productBaseSelector).hide();
}
function setEndOfProductListStatus(e, status) {
    endOfProductList = status;
}
function getEndOfProductListStatus(e, status) {
    return endOfProductList;
}
/* -------------------------------------------------------------------------------------------------------- */

$(document).ready(function () {
    $(document).bind('ProductJsonReceived', setEndOfProductListStatus);
    displayProducts();
});