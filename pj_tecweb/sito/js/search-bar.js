const searchBtnSelector = 'body > .container-fluid > main #searchInputBtn';
const searchInputSelector = 'body > .container-fluid > main #searchInput';

$(document).ready(function(){
    $(searchBtnSelector).click(function(e){
        e.preventDefault();
        resetIndexes();
        let searchQuery = $(searchInputSelector).val();
        displayProducts(currentIndex,searchQuery);
    });
});