$(document).ready(function () {

    var $errorDealerLicense = $("<div>").text("Per lasciare una recensione lasciare almeno una valutazione.").addClass("text-center text-danger");
    var $rating = -1;

    $("#ratingForm").submit(function (e) {
        if ($rating == -1) {
            e.preventDefault();
            $('#ratingForm').append($errorDealerLicense);
        }
    });

    $('input:radio[name="rating"]').change(function () {
        $rating = this.value;
        $('input:radio[name="rating"]').each(function () {
            let $label = $('#ratingForm label[for=' + this.id + ']');
            if (this.value <= $rating) {
                $label.css("color", "orange");
            } else {
                $label.css("color", "lightgray");
            }
        });
    });

});