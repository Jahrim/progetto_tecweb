const paymentBtnSelector = 'body > .container-fluid > main > div:first-child > div:nth-child(2) > a';

$(document).ready(function () {
    $(paymentBtnSelector).click(function(e) {
        e.preventDefault();
        $.ajax({url: $(this).attr('href')});
        $(this).text('Pagamento Effettuato');
        $(this).prop('disabled', true);
        removePreviousProducts();
    });
});
