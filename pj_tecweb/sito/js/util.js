var EnumQueryParameter = {
    ACTION : 'a',
    LIST_INDEX : 'idx',
    PRODUCT_ID : 'pid',
    NOTIFICATION_ID : 'nid',
    PRODUCT_LOCATION : 'loc',
    SEARCH_INPUT : 'srch'
}

var EnumAction = {
    LOGOUT : 1,
    ADD_TO_CART : 2,
    REMOVE_FROM_CART : 3,
    DELETE_PRODUCT : 4,
    END_PAYMENT : 5,
    DELETE_NOTIFICATION : 6
}

var EnumProductLocation = {
    ALL : 1,
    CART : 2,
    HISTORY : 3,
    SELL : 4
}

var EnumButtonIstance = {
    ADD_TO_CART : 'btnAddToCart',
    REMOVE_FROM_CART : 'btnRemoveFromCart',
    UPDATE_PRODUCT : 'btnUpdateProduct',
    DELETE_PRODUCT : 'btnDeleteProduct',
    SHOW_PRODUCT : 'btnShowProduct',
    RATE_PRODUCT : 'btnRateProduct'
}

function concatGET(...queryGETs){
    let query = '';
    $.each( queryGETs, function( index, value ){
        if (index != 0){ query+='&'; }
        query += value;
    });
    return query;
}
function queryGET(idParameter, value){
    return idParameter + '=' + value;
}

$.fn.exists = function () {
    return this.length !== 0;
}

function getProductLocation(){
    if (document.URL.includes("cart.php")) {
        return EnumProductLocation.CART;
    } else if (document.URL.includes("history.php")) {
        return EnumProductLocation.HISTORY;
    } else if (document.URL.includes("sell.php")) {
        return EnumProductLocation.SELL;
    }
    return EnumProductLocation.ALL;
}