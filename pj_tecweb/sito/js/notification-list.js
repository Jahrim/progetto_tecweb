const MAX_NOTIFICATION_LIST_SIZE = 30;

var endOfNotificationList = false;

const notificationListSelector = 'body > .container-fluid > main > table > tbody';
const notificationBaseSelector = notificationListSelector + '> tr:first-of-type';
const notificationClonesSelector = notificationListSelector + '> tr:not(:first-of-type)';
const notificationCloneObjectSelector = 'td:nth-child(1)';
const notificationCloneContentSelector = 'td:nth-child(2)';

function cloneBaseNotification() {
    $(notificationBaseSelector).show();
    let tempNotificationBase = $(notificationBaseSelector).clone()[0];
    $(notificationBaseSelector).hide();
    return tempNotificationBase;
}

function addButtonListeners(buttonClass) {
    let buttons = $('.' + buttonClass + ' > .btn', notificationListSelector);
    if (buttons.exists()) {
        buttons.each(function (index, element) {
            let notificationID = ($(this).attr('id') !== undefined) ? $(this).attr('id').substring(buttonClass.length + 1) : '';
            $(this).click(function (e) {
                e.preventDefault();
                $.ajax({ url: $(this).attr('href') + notificationID });
                $('#Notification\\:' + notificationID).remove();
            });
        });
    }
}

function getNotificationListHTML(notificationListData) {
    let notificationListHTML = '';
    let tempNotificationBase = cloneBaseNotification();
    notificationListData.forEach(notificationData => {
        $(tempNotificationBase).attr({ 'id': 'Notification:' + notificationData['notificationID'] })
        $(notificationCloneObjectSelector, tempNotificationBase).text(notificationData['subject']);
        $(notificationCloneContentSelector, tempNotificationBase).text(notificationData['content']);
        $('.deleteNotificationBtn > .btn', tempNotificationBase).attr({ 'id': ('deleteNotificationBtn:' + notificationData['notificationID']) });
        notificationListHTML += tempNotificationBase.outerHTML;
    });
    return notificationListHTML;
}

function displayNotifications(currentIndex = 0) {
    removePreviousNotifications();
    hideNotificationTemplate();
    let request = 'api/api-notification-list.php?' + queryGET(EnumQueryParameter.LIST_INDEX, currentIndex.toString());
    return $.getJSON(request, function (data) {
        if (data.length) {
            $(document).trigger('NotificationJsonReceived', false);
            let notificationListHTML = getNotificationListHTML(data);
            $(notificationListSelector).append(notificationListHTML);
            addButtonListeners('deleteNotificationBtn');
        } else {
            $(document).trigger('NotificationJsonReceived', true);
        }
    }); //.fail((jqXHR, textStatus, errorThrown) => alert(jqXHR + '   |   ' + textStatus + '   |   ' + errorThrown))
}
function removePreviousNotifications() {
    $(notificationClonesSelector).remove();
}
function hideNotificationTemplate() {
    $(notificationBaseSelector).hide();
}
function setEndOfNotificationListStatus(e, status) {
    endOfNotificationList = status;
}
function getEndOfNotificationListStatus(e, status) {
    return endOfNotificationList;
}
/* -------------------------------------------------------------------------------------------------------- */

$(document).ready(function () {
    $(document).bind('NotificationJsonReceived', setEndOfNotificationListStatus);
    displayNotifications();
});