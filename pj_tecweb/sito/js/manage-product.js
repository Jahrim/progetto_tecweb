$(document).ready(function () {
    var $error = $("<div>").css("color", "red").addClass("text-center text-danger");
    var $productImage = '';
    var $inputs = $('input').not(':input[type=hidden],:input[type=submit]');
    $inputs.each(function () {
        $(this).val($(this).attr('placeholder'));
        $(this).removeAttr("placeholder");
    });

    $("#manageProductForm").submit(function (e) {
        if ($('#name').val() == '' || $('#price').val() == '' || ($productImage == '' && $('#productImage').attr("src") == 'upload/product-img.png')) {
            e.preventDefault();
            $error.text("E' obbligatorio inserire almeno il nome, il prezzo e un immagine del prodotto!");
            $('#error').append($error);
        }
    });

    $('input[type="file"]').change(function (e) {
        $productImage = e.target.files[0].name;
        if ($productImage) {
            var reader = new FileReader();

            reader.onload = function (i) {
                $('#productImage')
                    .attr('src', i.target.result)
                    .width(255)
                    .height(255);
            };

            reader.readAsDataURL(e.target.files[0]);
        }
    });

});

