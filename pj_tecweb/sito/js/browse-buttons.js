const prevBtnProductSelector = 'body > .container-fluid > main > div:nth-child(3) #prevSearch';
const nextBtnProductSelector = 'body > .container-fluid > main > div:nth-child(3) #nextSearch';
const prevBtnNotificationSelector = 'body > .container-fluid > main > table + div #prevSearch';
const nextBtnNotificationSelector = 'body > .container-fluid > main > table + div #nextSearch';

var currentIndex = 0;
var maxIndex = 0;

function resetIndexes(){
    currentIndex = 0;
    maxIndex = 10000;
}

$(document).ready(function(){
    resetIndexes();
    if (document.URL.includes("notification.php")){
        $(prevBtnNotificationSelector).click(function(e){
            e.preventDefault();
            if (currentIndex > 0) { 
                currentIndex--; 
                displayNotifications(currentIndex);
            }
        });
        $(nextBtnNotificationSelector).click(function(e){
            e.preventDefault();
            if (currentIndex < maxIndex-1) { 
                currentIndex++; 
                displayNotifications(currentIndex).then(function(){
                    if (getEndOfNotificationListStatus() == true){
                        maxIndex = currentIndex;
                        $(prevBtnNotificationSelector).click();
                    }   
                });
            }
        });
    } else {
        $(prevBtnProductSelector).click(function(e){
            e.preventDefault();
            if (currentIndex > 0) { 
                currentIndex--; 
                displayProducts(currentIndex,currentSearch);
            }
            console.log(currentIndex + '   ' + maxIndex);
        });
        $(nextBtnProductSelector).click(function(e){
            e.preventDefault();
            if (currentIndex < maxIndex-1) { 
                currentIndex++; 
                displayProducts(currentIndex,currentSearch).then(function(){
                    if (getEndOfProductListStatus() == true){
                        maxIndex = currentIndex;
                        $(prevBtnProductSelector).click();
                    }
                });
            }
            console.log(currentIndex + '   ' + maxIndex);
        });
    }
});