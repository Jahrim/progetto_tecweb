<?php

/**
 * Static class to retrieve access information regarding this session user.
 */
final class UserHelper {
    const USERS = array(DatabaseHelper::USER_ROLE, DatabaseHelper::VENDOR_ROLE, DatabaseHelper::ADMIN_ROLE);
    const VENDORS = array(DatabaseHelper::VENDOR_ROLE, DatabaseHelper::ADMIN_ROLE);
    const ADMINS = array(DatabaseHelper::ADMIN_ROLE);

    /**
     * Saves the user data $userData for this session.
     */
    public static function saveSessionUserData($userData){
        $_SESSION["userID"] = $userData["userID"];
        $_SESSION["role"] = $userData["role"];
        $_SESSION["name"] = $userData["name"];
        $_SESSION["email"] = $userData["email"];
        UserHelper::setOptionalData($userData, 'phone');
        UserHelper::setOptionalData($userData, 'address');
        UserHelper::setOptionalData($userData, 'creditCard');
        UserHelper::setOptionalData($userData, 'companyID');
        UserHelper::setOptionalData($userData, 'IVA');
        UserHelper::setOptionalData($userData, 'companyPEC');
    }
    /**
     * Deletes all the user data of this session.
     */
    public static function deleteSessionUserData(){
        unset($_SESSION["userID"]);
        unset($_SESSION["role"]);
        unset($_SESSION["name"]);
        unset($_SESSION["email"]);
        unset($_SESSION["phone"]);
        unset($_SESSION["address"]);
        unset($_SESSION["creditCard"]);
        unset($_SESSION["companyID"]);
        unset($_SESSION["IVA"]);
        unset($_SESSION["companyPEC"]);
    }

    /**
     * Returns the user identifier of the user of this session.
     */
    public static function getUserID(){
        return $_SESSION["userID"];
    }
    /**
     * Returns the role of the user of this session.
     */
    public static function getUserRole(){
        return $_SESSION["role"];
    }
    /**
     * Returns the name of the user of this session.
     */
    public static function getUserName(){
        return $_SESSION["name"];
    }
    /**
     * Returns the email of the user of this session.
     */
    public static function getUserEmail(){
        return $_SESSION["email"];
    }
    /**
     * Returns the phone number of the user of this session
     * or an empty string if it isn't registered.
     */
    public static function getUserPhoneNumber(){
        return UserHelper::getOptionalData('phone');
    }
    /**
     * Returns the address of the user of this session
     * or an empty string if it isn't registered.
     */
    public static function getUserAddress(){
        return UserHelper::getOptionalData('address');
    }
    /**
     * Returns the phone number of the user of this session
     * or an empty string if it isn't registered.
     */
    public static function getUserCreditCard(){
        return UserHelper::getOptionalData('creditCard');
    }
    /**
     * Returns the company identifier of the company where the user of this session works
     * or an empty string if it isn't registered.
     */
    public static function getUserCompanyID(){
        return UserHelper::getOptionalData('companyID');
    }
    /**
     * Returns the company IVA of the company where the user of this session works
     * or an empty string if it isn't registered.
     */
    public static function getUserCompanyIVA(){
        return UserHelper::getOptionalData('IVA');
    }
    /**
     * Returns the company PEC of the company where the user of this session works
     * or an empty string if it isn't registered.
     */
    public static function getUserCompanyPEC(){
        return UserHelper::getOptionalData('companyPEC');
    }

    /**
     * Return true if the user is logged in.
     */
    public static function isLoggedIn(){
        return !empty($_SESSION["userID"]);
    }
    /**
     * Return true if the user has user rights.
     */
    public static function isUser(){
        return UserHelper::isLoggedIn() && in_array($_SESSION["role"], UserHelper::USERS);
    }
    /**
     * Return true if the user has vendor rights.
     */
    public static function isVendor(){
        return UserHelper::isLoggedIn() && in_array($_SESSION["role"], UserHelper::VENDORS);
    }
    /**
     * Return true if the user has admin rights.
     */
    public static function isAdmin(){
        return UserHelper::isLoggedIn() && in_array($_SESSION["role"], UserHelper::ADMINS);
    }
    /**
     * Return true if the user has registed the data needed to buy a product.
     */
    public static function canBuy(){
        return !empty($_SESSION["phone"]) || 
               !empty($_SESSION["address"]) ||
               !empty($_SESSION["creditCard"]);
    }

    /**
     * Saves the data contained in $userData[$key] in the current session if it exists.
     */
    private static function setOptionalData($userData, $key){
        if (isset($userData[$key])){
            $_SESSION[$key] = $userData[$key];
        }
    }
    /**
     * Retrieve the data contained in $_SESSION[$key] if it exists.
     */
    private static function getOptionalData($key){
        if (isset($_SESSION[$key])){
            return $_SESSION[$key];
        } else {
            return '';
        }
    }
}

?>