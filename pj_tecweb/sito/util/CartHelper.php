<?php

/**
 * Static class used to access cart properties.
 */
final class CartHelper {

    public static function isCartEmpty(){
        return !isset($_COOKIE['cart']);
    }

    public static function getCart(){
        return CartHelper::isCartEmpty() ? array() : json_decode($_COOKIE['cart']);
    }

    public static function addToCart(...$productIDs){
        $productsInCart = CartHelper::getCart();
        foreach($productIDs as $productID){
            if (CartHelper::isCartEmpty() || !in_array($productID, $productsInCart)) { 
                array_push($productsInCart, $productID);
            }
        }
        setcookie('cart', json_encode($productsInCart), time()+3600, '/');
    }

    public static function removeFromCart(...$productIDs){
        $newProductsInCart = array();
        foreach (CartHelper::getCart() as $cartProductID){
            if (!in_array($cartProductID, $productIDs)) {
                array_push($newProductsInCart, $cartProductID);
            }
        }
        if (empty($newProductsInCart)){
            CartHelper::deleteCart();
        } else {
            setcookie('cart', json_encode($newProductsInCart), time()+3600, '/');
        }
    }

    public static function deleteCart(){
        setcookie('cart', '', time()-3600, '/');
        unset($_COOKIE['cart']);
    }

}
?>