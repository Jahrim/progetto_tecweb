<?php
final class ImageHandler
{
    public static function uploadImage($path, $image)
    {
        $result = 0;
        $msg = "";
        if (isset($image["name"]) && $image["tmp_name"]) {
            $imageName = basename($image["name"]);
            $fullPath = $path . $imageName;

            $maxKB = 500;
            $acceptedExtensions = array("jpg", "jpeg", "png");
            //Check if the image is real
            $imageSize = getimagesize($image["tmp_name"]);
            if ($imageSize === false) {
                $msg .= "Il file caricato non è un'immagine!";
            }

            //Check if the image size is < 500KB
            if ($image["size"] > $maxKB * 1024) {
                $msg .= " Il file caricato supera la dimensione massima di $maxKB KB.";
            }

            //Check file extension
            $imageFileType = strtolower(pathinfo($fullPath, PATHINFO_EXTENSION));
            if (!in_array($imageFileType, $acceptedExtensions)) {
                $msg .= " Sono accettate solo le seguenti estensioni: " . implode(", ", $acceptedExtensions) . ".";
            }

            //Check if a file with the same name actually exist, in case rename it.
            if (file_exists($fullPath)) {
                $i = 1;
                do {
                    $i++;
                    $imageName = pathinfo(basename($image["name"]), PATHINFO_FILENAME) . "_$i." . $imageFileType;
                } while (file_exists($path . $imageName));
                $fullPath = $path . $imageName;
            }

            //If no error occures, move the file from temporary path to the destination path
            if (strlen($msg) == 0) {
                if (!move_uploaded_file($image["tmp_name"], $fullPath)) {
                    $msg .= " Errore nel caricamento dell'immagine.";
                } else {
                    $result = 1;
                    $msg = $imageName;
                }
            }
        }
        return array($result, $msg);
    }
}
