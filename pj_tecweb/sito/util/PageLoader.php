<?php

/**
 * Enum of the pages containing their respective file names.
 */
abstract class Page {
    const INDEX = 'page-index.php';
    const CART = 'page-cart.php';
    const HISTORY = 'page-history.php';
    const NOTIFICATION = 'page-notification.php';
    const LOGIN = 'page-login.php';
    const SIGNIN = 'page-signin.php';
    const MANAGE_PRODUCT = 'page-manage-product.php';
    const PROFILE = 'page-profile.php';
    const SELL = 'page-sell.php';
    const PRODUCT = 'page-product.php';
}

const PAGE_REL_PATH = '/../template/pages/';

require_once dirname(__FILE__) . PAGE_REL_PATH . Page::INDEX;
require_once dirname(__FILE__) . PAGE_REL_PATH . Page::CART;
require_once dirname(__FILE__) . PAGE_REL_PATH . Page::HISTORY;
require_once dirname(__FILE__) . PAGE_REL_PATH . Page::NOTIFICATION;
require_once dirname(__FILE__) . PAGE_REL_PATH . Page::LOGIN;
require_once dirname(__FILE__) . PAGE_REL_PATH . Page::SIGNIN;
require_once dirname(__FILE__) . PAGE_REL_PATH . Page::MANAGE_PRODUCT;
require_once dirname(__FILE__) . PAGE_REL_PATH . Page::PROFILE;
require_once dirname(__FILE__) . PAGE_REL_PATH . Page::SELL;
require_once dirname(__FILE__) . PAGE_REL_PATH . Page::PRODUCT;

/**
 * Static class used to load the pages of the server.
 */
final class PageLoader {
    /**
     * Loads the page specified in the template parameter $tp['page'] and passes the
     * template parameters to the page to be loaded.
     * The specified page must be in the enum Page.
     */
    public static function load($tp){
        switch ($tp['page']) {
            case Page::INDEX:
                return (new PageIndex($tp));
            case Page::CART:
                return (new PageCart($tp));
            case Page::HISTORY:
                return (new PageHistory($tp));
            case Page::NOTIFICATION:
                return (new PageNotification($tp));
            case Page::LOGIN:
                return (new PageLogin($tp));
            case Page::SIGNIN:
                return (new PageSignIn($tp));
            case Page::MANAGE_PRODUCT:
                return (new PageManageProduct($tp));
            case Page::PROFILE:
                return (new PageProfile($tp));
            case Page::SELL:
                return (new PageSell($tp));
            case Page::PRODUCT:
                return (new PageProduct($tp));
            default:
                die("PageLoader.load : the specified page is not yet implemented.");
        }
    }
}

?>