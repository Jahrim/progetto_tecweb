<?php

/**
 * Enum of the available query parameters in this webpage.
 */
abstract class QueryParameter
{
    const ACTION = 'a';
    const LIST_INDEX = 'idx';
    const PRODUCT_ID = 'pid';
    const NOTIFICATION_ID = 'nid';
    const PRODUCT_LOCATION = 'loc';
    const SEARCH_INPUT = 'srch';
    const STATUS = 'scs';

    public static function concatGET(...$queryGETs)
    {
        $query = '';
        for ($i = 0; $i < count($queryGETs); $i++) {
            if ($i != 0) {
                $query .= '&';
            }
            $query .= $queryGETs[$i];
        }
        return $query;
    }
    public static function queryGET($queryParameter, $value)
    {
        return $queryParameter . '=' . strval($value);
    }
}

abstract class Status
{
    const UPDATE_SUCCESS = 1;
    const INSERT_SUCCESS = 2;
}

/**
 * Enum of the available actions in this webpage.
 */
abstract class Action
{
    const LOGOUT = 1;
    const ADD_TO_CART = 2;
    const REMOVE_FROM_CART = 3;
    const DELETE_PRODUCT = 4;
    const END_PAYMENT = 5;
    const DELETE_NOTIFICATION = 6;
}

/**
 * Enum of the available product location in this webpage.
 */
abstract class ProductLocation
{
    const ALL = 1;
    const CART = 2;
    const HISTORY = 3;
    const SELL = 4;
}
