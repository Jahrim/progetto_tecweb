<?php
require_once 'init.php';

if (UserHelper::isVendor()) {
    /* PAGE DATA -------------------------------------------------------------------------*/
    if (isset($_POST["name"]) && isset($_POST["price"]) && isset($_FILES["uploadedImage"])) {
        //update existing product
        if (isset($_POST["productID"]) && $_POST["productID"] != '') {
            if (($_FILES["uploadedImage"]["name"]) != "") {
                list($result, $msg) = ImageHandler::uploadImage(UPLOAD_DIR, $_FILES["uploadedImage"]);
                if ($result != 0) {
                    $databaseHelper->updateProduct($_POST["productID"], $_POST["name"], $msg, $_POST["shortDescription"] ?? '', $_POST["longDescription"] ?? '', $_POST["price"]);
                    $status = Status::UPDATE_SUCCESS;
                    header("location: sell.php?" . QueryParameter::queryGET(QueryParameter::STATUS, $status));
                    return;
                } else {
                    $tp["imageError"] = $msg ?? '';
                }
                //update product without new image
            } else {
                $databaseHelper->updateProduct($_POST["productID"], $_POST["name"], $msg, $_POST["shortDescription"] ?? '', $_POST["longDescription"] ?? '', $_POST["price"]);
                $status = Status::UPDATE_SUCCESS;
                header("location: sell.php?" . QueryParameter::queryGET(QueryParameter::STATUS, $status));
                return;
            }
            //add new product
        } else {
            list($result, $msg) = ImageHandler::uploadImage(UPLOAD_DIR, $_FILES["uploadedImage"]);
            if ($result != 0) {
                $databaseHelper->insertProduct($_POST["name"], $msg, $_POST["shortDescription"] ?? '', $_POST["longDescription"] ?? '', $_POST["price"], $_SESSION["userID"]);
                $status = Status::INSERT_SUCCESS;
                header("location: sell.php?" . QueryParameter::queryGET(QueryParameter::STATUS, $status));
                return;
            } else {
                $tp["imageError"] = $msg ?? '';
            }
        }
    }
    $tp["css"] = array(
        "external/bootstrap-v4.5.3.css",
        "css/components/comp-header-footer.css"
    );
    $tp["icon"] = array(
        "https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css"
    );
    $tp["js"] = array(
        "external/jquery-v3.5.1.js",
        "external/bootstrap.bundle.v4.5.3.js",
        "js/manage-product.js"
    );
    $tp["meta-title"] = "Shopzone - Gestisci Prodotto";
    $tp["page"] = Page::MANAGE_PRODUCT;
    $tp["product"] = isset($_GET[QueryParameter::PRODUCT_ID]) ? $databaseHelper->getProducts(array($_GET[QueryParameter::PRODUCT_ID]), false, false)[0] : null;
    $tp["title"] = $tp['product'] ? "Modifica Prodotto" : "Aggiungi Prodotto";
    require 'template/base.php';
} else {
    header("location: index.php");
}
