<?php
session_start();
define("UPLOAD_DIR", "upload/");

require_once("db/database.php");
require_once("util/Constants.php");
require_once("util/QueryParameter.php");
require_once("util/PageLoader.php");
require_once("util/UserHelper.php");
require_once("util/CartHelper.php");
require_once("util/ImageHandler.php");

$databaseHelper = new DatabaseHelper("localhost", "root", "", "Shopzone", 3306);
