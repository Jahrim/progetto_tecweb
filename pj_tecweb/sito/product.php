<?php
require_once 'init.php';

if (isset($_GET[QueryParameter::PRODUCT_ID]) || isset($_POST[QueryParameter::PRODUCT_ID])) {
    /* IMPORT ---------------------------------------------------------------------------*/
    $tp["css"] = array(
        "external/bootstrap-v4.5.3.css",
        "css/components/comp-header-footer.css",
        "css/pages/page-product.css"
    );
    $tp["icon"] = array(
        "https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css"
    );
    $tp["js"] = array(
        "external/jquery-v3.5.1.js",
        "external/bootstrap.bundle.v4.5.3.js",
        "js/page-product.js"
    );
    /* PAGE DATA -------------------------------------------------------------------------*/
    $tp["canReview"] = true;
    $productID = isset($_GET[QueryParameter::PRODUCT_ID]) ? $_GET[QueryParameter::PRODUCT_ID] : $_POST[QueryParameter::PRODUCT_ID];
    $tp["product"] = $databaseHelper->getProducts(array($productID), false, false)[0];
    //check if the user hasn't the product in his history, to prevent him reviewing it.
    $userHistoryProduct = $databaseHelper->getHistoryProducts($_SESSION["userID"]);
    if (!isset(array_column($userHistoryProduct, "name", "productID")[$productID])) {
        $tp["canReview"] = false;
        //check if the user has already reviewed the product
    } else if (count($databaseHelper->getUserReviewOfProduct($productID, $_SESSION["userID"])) != 0) {
        $tp["canReview"] = false;
        //check if the user want to submit a review for the product
    } else if (isset($_POST["rating"])) {
        $databaseHelper->reviewProduct($productID, $_SESSION["userID"], $_POST["rating"], $_POST["personalReview"] ?? '');
        $tp["hasReviewed"] = "La tua recensione e' stata inserita con successo!";
        $tp["canReview"] = false;
        //notify the vendor that his product has been reviewed
        $databaseHelper->insertNotification($tp["product"]["vendorID"], "Ricevuta Recensione", "Il prodotto " . $tp["product"]["name"] . " ha ricevuto " . $_POST["rating"] . ($_POST["rating"] == 1 ? " stella." : " stelle."));
        //retrieve updated product
        $tp["product"] = $databaseHelper->getProducts(array($productID), false, false)[0];
    }

    $tp["meta-title"] = "Shopzone - Prodotto";
    $tp["title"] = "Visualizza Prodotto";
    $tp["page"] = Page::PRODUCT;
    $tp["vendor"] = $databaseHelper->getUserByID($tp["product"]["vendorID"]);
    $tp['reviews'] = $databaseHelper->getProductReviews($productID);

    require 'template/base.php';
} else {
    die('product.php : error requested product is not available');
}
