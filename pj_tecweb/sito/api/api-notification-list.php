<?php

require_once dirname(__FILE__) . '/../init.php';

if (isset($_GET[QueryParameter::LIST_INDEX])) {
    $notificationDataList = array();
    if (UserHelper::isLoggedIn()){
        $notificationDataList = $databaseHelper->getUserNotifications(UserHelper::getUserID());
    }
    if (!empty($notificationDataList)){
        $startingIndex = $_GET[QueryParameter::LIST_INDEX];
        $notificationDataList = array_slice($notificationDataList, $startingIndex*C::MAX_NOTIFICATION_LIST_LENGTH, C::MAX_NOTIFICATION_LIST_LENGTH);
        header('Content-Type: application/json');
        echo json_encode($notificationDataList);
    }
    return;
}

if (isset($_GET[QueryParameter::ACTION])){
    switch ($_GET[QueryParameter::ACTION]){
        case Action::DELETE_NOTIFICATION:
            if (isset($_GET[QueryParameter::NOTIFICATION_ID])){
                $databaseHelper->removeNotification($_GET[QueryParameter::NOTIFICATION_ID]);
            }
            break;
        default: die('api-notification-list.php : error action not supported.');
    }
}

?>
