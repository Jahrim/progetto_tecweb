<?php

require_once dirname(__FILE__) . '/../init.php';

function satisfySearch($productData, $searchInput)
{
    return $productData['productID'] == $searchInput ||
        strpos($productData['name'], $searchInput) !== false ||
        strpos($productData['shortDescription'], $searchInput) !== false ||
        strpos($productData['longDescription'], $searchInput) !== false;
}
if (isset($_GET[QueryParameter::LIST_INDEX]) && isset($_GET[QueryParameter::PRODUCT_LOCATION])) {
    $productDataList = array();
    switch ($_GET[QueryParameter::PRODUCT_LOCATION]) {
        case ProductLocation::ALL:
            $productDataList = $databaseHelper->getProducts();
            break;
        case ProductLocation::CART:
            $productDataList = (UserHelper::isLoggedIn() && !CartHelper::isCartEmpty()) ? $databaseHelper->getProducts(CartHelper::getCart(), $excludeCart = false) : array();
            break;
        case ProductLocation::HISTORY:
            $productDataList = (UserHelper::isLoggedIn()) ? $databaseHelper->getHistoryProducts(UserHelper::getUserID()) : array();
            break;
        case ProductLocation::SELL:
            $productDataList = (UserHelper::isVendor()) ? $databaseHelper->getVendorProducts(UserHelper::getUserID()) : array();
            break;
        default:
            die('api-product-list.php : error product location not supported.');
    }
    if (!empty($productDataList)) {
        $startingIndex = $_GET[QueryParameter::LIST_INDEX];
        if (isset($_GET[QueryParameter::SEARCH_INPUT]) && $_GET[QueryParameter::SEARCH_INPUT] != '') {
            $searchedProductData = array();
            foreach ($productDataList as $productData) {
                if (satisfySearch($productData, $_GET[QueryParameter::SEARCH_INPUT])) {
                    array_push($searchedProductData, $productData);
                }
            }
            $productDataList = $searchedProductData;
        }
        $productDataList = array_slice($productDataList, $startingIndex * C::MAX_PRODUCT_LIST_LENGTH, C::MAX_PRODUCT_LIST_LENGTH);
        for ($i = 0; $i < count($productDataList); $i++) {
            $productDataList[$i]['image'] = UPLOAD_DIR . $productDataList[$i]['image'];
        }
        header('Content-Type: application/json');
        echo json_encode($productDataList);
    }
    return;
}

if (isset($_GET[QueryParameter::ACTION])) {
    switch ($_GET[QueryParameter::ACTION]) {
        case Action::ADD_TO_CART:
            if (isset($_GET[QueryParameter::PRODUCT_ID])) {
                CartHelper::addToCart($_GET[QueryParameter::PRODUCT_ID]);
            }
            break;
        case Action::REMOVE_FROM_CART:
            if (isset($_GET[QueryParameter::PRODUCT_ID])) {
                CartHelper::removeFromCart($_GET[QueryParameter::PRODUCT_ID]);
            }
            break;
        case Action::DELETE_PRODUCT:
            if (isset($_GET[QueryParameter::PRODUCT_ID])) {
                $databaseHelper->updateProductAvailability($_GET[QueryParameter::PRODUCT_ID], false);
            }
            break;
        case Action::END_PAYMENT:
            if (!CartHelper::isCartEmpty()) {
                foreach (CartHelper::getCart() as $productID) {
                    $productData = $databaseHelper->getProducts(array($productID), $excludeCart = false)[0];
                    $databaseHelper->insertPurchase(UserHelper::getUserID(), $productData['productID']);
                    $databaseHelper->insertNotification(UserHelper::getUserID(), 'Acquisto Prodotto', 'Avete acquistato \'' . $productData['name'] . '\' con successo!');
                    $databaseHelper->insertNotification($productData['vendorID'], 'Vendita Prodotto', 'E\' stata venduta una copia di \'' . $productData['name'] . '\' con successo!');
                }
                CartHelper::deleteCart();
            }
            break;
        default:
            die('api-product-list.php : error action not supported.');
    }
}
