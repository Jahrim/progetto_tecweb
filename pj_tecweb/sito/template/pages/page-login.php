<?php
require_once dirname(__FILE__) . '/../components/main.php';
require_once dirname(__FILE__) . '/../components/advanced/input.php';

/**
 * This HTMLComponent represents the main content of the page login.html.
 */
class PageLogin extends Main
{
    public function __construct($tp)
    {
        parent::__construct();
        $this->addHTML(<<<HTML
            <div class="container justify-content-center">
                <form action="#" method="POST">
                    <div class="text-center">
                        <figure>
                            <img alt="" src="upload/logo.png" />
                        </figure>
                    </div>
                    <fieldset>
        HTML)->addHTML(
            (new Input("email", "E-mail", "email", InputStyle::NOT_LOGGED))->close()->getHTML(),
            (new Input("password", "Password", "password", InputStyle::NOT_LOGGED))->close()->getHTML(),
            (new Input("submit", "Accedi", "submit", InputStyle::NOT_LOGGED))->close()->getHTML()
        )->addHTML(<<<HTML
                    </fieldset>
                </form>
            </div>
        HTML);
    }
}
