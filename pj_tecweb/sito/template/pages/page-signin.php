<?php
require_once dirname(__FILE__) . '/../components/main.php';

/**
 * This HTMLComponent represents the main content of the page signin.html.
 */
class PageSignIn extends Main
{
    public function __construct($tp)
    {
        parent::__construct();
        $this->addHTML(<<<HTML
            <div class="container justify-content-center">
                <form action="#" method="POST">
                    <div class="text-center">
                        <figure>
                            <img alt="" src="upload/logo.png" />
                        </figure>
                    </div>
                    <fieldset>
        HTML)->addHTML(
            (new Input("name", "Nome", "text", InputStyle::NOT_LOGGED))->close()->getHTML(),
            (new Input("email", "E-mail", "email", InputStyle::NOT_LOGGED))->close()->getHTML(),
            (new Input("password", "Password", "password", InputStyle::NOT_LOGGED))->close()->getHTML(),
            (new Input("confirmPassword", "Conferma Password", "password", InputStyle::NOT_LOGGED))->close()->getHTML(),
            (new Input("submit", "Registrati", "submit", InputStyle::NOT_LOGGED))->close()->getHTML()
        )->addHTML(<<<HTML
                    </fieldset>
                </form>
            </div>
        HTML);
    }
}
