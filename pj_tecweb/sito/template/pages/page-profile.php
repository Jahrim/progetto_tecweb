<?php
require_once dirname(__FILE__) . '/../components/main.php';
require_once dirname(__FILE__) . '/../components/advanced/input.php';

/**
 * This HTMLComponent represents the main content of the page profile.html.
 */
class PageProfile extends Main
{
    public function __construct($tp)
    {
        $wrongPasswordError = $tp["errorWrongPassword"] ?? '';
        parent::__construct();
        $this->addHTML(<<<HTML
            <div class="container justify-content-center">
                <form id="updateDataForm" action="#" method="POST">
                    <fieldset>
                        <legend>Informazioni di Base</legend>
        HTML)->addHTML(
            (new Input("name", "Nome", "text", InputStyle::LOGGED_DEFAULT, $_SESSION["name"]))->close()->getHTML(),
            (new Input("password", "Password", "password", InputStyle::LOGGED_WITH_BUTTON, "********", "#passwordUpdateModal", "Modifica Password"))->close()->getHTML(),
        )->conditionalAddHTML($wrongPasswordError, <<<HTML
                        <div class="text-center text-danger">{$wrongPasswordError}</div>
        HTML)->conditionalAddHTML(isset($tp["updateUserPasswordSuccess"]), <<<HTML
                         <div class="text-center text-success">{$tp["updateUserPasswordSuccess"]}</div>
        HTML)->addHTML(<<<HTML
                    </fieldset>
                    <fieldset id="errorMissingEmail">
                        <legend>Dati Utente</legend>
        HTML)->addHTML(
            (new Input("email", "E-mail", "email", InputStyle::LOGGED_DEFAULT, $_SESSION["email"]))->close()->getHTML(),
            (new Input("phone", "Telefono", "tel", InputStyle::LOGGED_DEFAULT, $_SESSION["phone"] ?? ''))->close()->getHTML(),
            (new Input("address", "Indirizzo", "text", InputStyle::LOGGED_DEFAULT, $_SESSION["address"] ?? ''))->close()->getHTML(),
            (new Input("creditCard", "Carta di Credito", "number", InputStyle::LOGGED_DEFAULT, $_SESSION["creditCard"] ?? ''))->close()->getHTML(),
            (new Input("updateData", "Aggiorna Dati", "submit", InputStyle::LOGGED_DEFAULT))->close()->getHTML(),
            (new Input("confirm", "Conferma", "submit", InputStyle::LOGGED_DEFAULT))->close()->getHTML()
        )->conditionalAddHTML(isset($tp["updateUserDataSuccess"]), <<<HTML
                        <div class="text-center text-success">{$tp["updateUserDataSuccess"]}</div>
        HTML)->addHTML(<<<HTML
                    </fieldset>
                    <fieldset>
                        <legend>Dati Negoziante</legend>
        HTML)->conditionalAddHTML(
            !isset($_SESSION["companyID"]),
            (new Input("dealerLicense", "Licenza da Negoziante", "text", InputStyle::LOGGED_WITH_BUTTON, isset($_SESSION["companyID"]) ? "Si" : "No", "#dealerLicenseModal", "Ottieni Licenza"))->close()->getHTML()
        )->addHTML(
            (new Input("iva", "Partita Iva", "number", InputStyle::LOGGED_DEFAULT, $_SESSION["IVA"] ?? ''))->close()->getHTML(),
            (new Input("companyID", "Codice dell'Azienda", "text", InputStyle::LOGGED_DEFAULT, $_SESSION["companyID"] ?? ''))->close()->getHTML(),
            (new Input("pec", "PEC Aziendale", "text", InputStyle::LOGGED_DEFAULT, $_SESSION["companyPEC"] ?? ''))->close()->getHTML()
        )->conditionalAddHTML(isset($tp["unlockVendorLicenseSuccess"]), <<<HTML
                        <div class="text-center text-success">{$tp["unlockVendorLicenseSuccess"]}</div>
        HTML)->addHTML(<<<HTML
                    </fieldset>
                </form>
            </div>

            <!-- Dealer License Modal -->
            <div class="modal fade" id="dealerLicenseModal" tabindex="-1" role="dialog"
                aria-labelledby="dealerLicenseModal" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h2 class="modal-title">Ottieni Licenza</h2>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div id="errorDealerLicense" class="modal-body">
                            <div class="container justify-content-center">
                                <form id="dealerLicenseForm" action="#" method="POST">
                                    <fieldset>
                                        <legend></legend>
        HTML)->addHTML(
            (new Input("modalIva", "Partita IVA", "number", InputStyle::MODAL))->close()->getHTML(),
            (new Input("modalCompanyId", "Codice dell'Azienda", "text", InputStyle::MODAL))->close()->getHTML(),
            (new Input("modalPec", "PEC Aziendale", "text", InputStyle::MODAL))->close()->getHTML()
        )->addHTML(<<<HTML
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
                            <input form="dealerLicenseForm" type="submit" name="submit" value="Conferma" class="btn btn-primary" />
                        </div>
                    </div>
                </div>
            </div>
            <!-- Dealer License Modal -->
            <!-- Password Update Modal -->
            <div class="modal fade" id="passwordUpdateModal" tabindex="-1" role="dialog"
                aria-labelledby="passwordUpdateModal" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h2 class="modal-title">Modifica Password</h2>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div id="errorPassword" class="modal-body">
                            <div class="container justify-content-center">
                                <form id="updatePasswordForm" action="#" method="POST">
                                    <fieldset>
                                        <legend></legend>
        HTML)->addHTML(
            (new Input("modalPassword", "Nuova Password", "password", InputStyle::MODAL))->close()->getHTML(),
            (new Input("modalPasswordConfirm", "Conferma Password", "password", InputStyle::MODAL))->close()->getHTML(),
            (new Input("modalLastPassword", "Password Precedente", "password", InputStyle::MODAL))->close()->getHTML()
        )->addHTML(<<<HTML
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
                            <input form="updatePasswordForm" type="submit" name="submit" value="Conferma" class="btn btn-primary" />
                        </div>
                    </div>
                </div>
            </div>
        HTML);
    }
}
