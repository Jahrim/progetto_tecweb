<?php
require_once dirname(__FILE__) . '/../components/main.php';
require_once dirname(__FILE__) . '/../components/advanced/notification-comp.php';

/**
 * This HTMLComponent represents the main content of the page notification.html.
 */
class PageNotification extends Main
{
    public function __construct($tp)
    {
        parent::__construct();
        $notification = $tp["notification"];
        $this->addHTML(<<<HTML
                            <table id="notificationTable" class="table border" aria-labelledby="notificationTable">
                            <thead>
                                <tr>
                                    <th id="object" scope="col">Oggetto</th>
                                    <th id="desc" scope="col">Descrizione Sintetica</th>
                                    <th id="actions" scope="col">Azioni Disponibili</th>
                                </tr>
                            </thead>
                            <tbody>
                        HTML,
                            (new NotificationList(array(Notification::EMPTY_PRODUCT_DATA)))->close()->getHTML(),
                        <<<HTML
                            </tbody>
                            </table>
                        HTML,
                            (new Row("justify-content-center"))->addHTML(
                                (new BrowseButtons("prevSearch", "nextSearch", "col-10 col-lg-6 text-center"))->close()->getHTML()
                            )->close()->getHTML()
        );
    }
}
