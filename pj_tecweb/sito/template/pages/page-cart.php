<?php
require_once dirname(__FILE__) . '/../components/main.php';
require_once dirname(__FILE__) . '/../components/advanced/research-bar.php';
require_once dirname(__FILE__) . '/../components/advanced/product-card.php';
require_once dirname(__FILE__) . '/../components/advanced/browse-buttons.php';

/**
 * This HTMLComponent represents the main content of the page cart.html.
 */
class PageCart extends Main
{
    public function __construct($tp)
    {
        parent::__construct();
        $totalCartCost = $tp['totalCartPrice'];
        $payQuery = QueryParameter::queryGET(QueryParameter::ACTION, Action::END_PAYMENT);
        $this->addHTML(
            (new Row("justify-content-center align-items-center"))->addHTML(<<<HTML
                <div class="col-6 col-lg-4 form-group">
                    <h2><strong>Prezzo Totale: <br>{$totalCartCost}€</strong></h2>
                </div>   
                HTML,
                (Button::build(ButtonInstance::END_PAYMENT, "api/api-product-list.php?".$payQuery, "col-6 col-lg-4 form-group"))->close()->getHTML()
            )->close()->getHTML(),
            (new Row("justify-content-center"))->addHTML(<<<HTML
                <h2>Dettagli Ordine</h2>
                HTML,
                (new ProductCardList(ProductType::CART_PRODUCT, array(ProductCard::EMPTY_PRODUCT_DATA), false))->close()->getHTML()
            )->close()->getHTML(),
            (new Row("justify-content-center"))->addHTML(
                (new BrowseButtons("prevSearch", "nextSearch", "col-10 col-lg-6 text-center"))->close()->getHTML()
            )->close()->getHTML()
        );
    }
}
