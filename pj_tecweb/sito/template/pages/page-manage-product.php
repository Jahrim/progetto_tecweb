<?php
require_once dirname(__FILE__) . '/../components/main.php';

/**
 * This HTMLComponent represents the main content of the page manage_product.html.
 */
class PageManageProduct extends Main
{
    public function __construct($tp)
    {
        $product = $tp['product'] ?? '';
        $name = $product['name'] ?? '';
        $imageName = $product['image'] ?? '';
        $image = $imageName != '' ? UPLOAD_DIR . $imageName : UPLOAD_DIR . "product-img.png";
        $tp["oldImage"] = $image ?? '';
        $shortDescription = $product['shortDescription'] ?? '';
        $longDescription = $product['longDescription'] ?? '';
        $price = $product['price'] ?? '';
        $buttonName = $product != '' ? "Salva Modifiche" : "Aggiungi Prodotto";
        $imageError = $tp["imageError"] ?? '';
        parent::__construct();
        $this->addHTML(<<<HTML
            <div id="error" class="container justify-content-center">
                <form id="manageProductForm" action="#" method="POST" enctype="multipart/form-data">
                    <fieldset>
                        <legend></legend>
        HTML)->addHTML(
            (new Input("name", "Nome", "text", InputStyle::NOT_LOGGED, $name))->close()->getHTML(),
            (new Input("shortDescription", "Descrizione Sintetica", "textarea", InputStyle::NOT_LOGGED, $shortDescription))->close()->getHTML(),
            (new Input("longDescription", "Descrizione Estesa", "textarea", InputStyle::NOT_LOGGED, $longDescription))->close()->getHTML(),
            (new Input("price", "Prezzo (€)", "number", InputStyle::NOT_LOGGED, $price))->close()->getHTML(),
            (new Input("uploadedImage", "Immagine", "file", InputStyle::FILE))->close()->getHTML(),
        )->addHTML(<<<HTML
                        <div class="text-center">
                            <figure>
                                <img id="productImage" alt="Immagine prodotto" src="${image}" width="255" height="255"/>
                            </figure>
                        </div>
        HTML)->addHTML(
            (new Input("submit", $buttonName, "submit", InputStyle::LOGGED_DEFAULT))->close()->getHTML(),
            (new Input("productID", $product["productID"] ?? '', "hidden", InputStyle::LOGGED_DEFAULT))->close()->getHTML()
        )->addHTML(<<<HTML
                    </fieldset>
                </form>
            </div>
        HTML)->conditionalAddHTML($imageError, <<<HTML
            <div id="imageError" class="col-12 text-center text-danger">$imageError</div>
        HTML);
    }
}
