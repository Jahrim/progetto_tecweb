<?php
require_once dirname(__FILE__) . '/../components/main.php';
require_once dirname(__FILE__) . '/../components/advanced/research-bar.php';
require_once dirname(__FILE__) . '/../components/advanced/product-card.php';
require_once dirname(__FILE__) . '/../components/advanced/browse-buttons.php';

/**
 * This HTMLComponent represents the main content of the page sell.html.
 */
class PageSell extends Main
{
    public function __construct($tp)
    {
        $missingData = $tp["missingData"] ?? '';
        $successAction = $tp["successAction"] ?? '';
        $size = $missingData ? "col-lg-12" : "col-lg-8";
        parent::__construct();
        $this->addHTML(
            (new Row())->addHTML(
                (new ResearchBar("searchInput", "Cerca un tuo prodotto in vendita...", "col-12 {$size} form-group"))->close()->getHTML()
            )->conditionalAddHTML(
                !$missingData,
                (new Button(ButtonType::LINK_BUTTON, ButtonStyle::INTERACTIVE_BLOCK, "Aggiungi un Prodotto", "./manage-product.php", "col-12 mb-4 col-lg-4 mt-lg-4 mb-lg-0 align-items-center"))->close()->getHTML()
            )->conditionalAddHTML($missingData, <<<HTML
                    <div class="col-12 text-center text-danger">{$missingData}</div>
            HTML)->conditionalAddHTML($successAction, <<<HTML
                    <div class="col-12 text-center text-success">{$successAction}</div>
            HTML)->close()->getHTML(),
            (new Row("justify-content-center"))->addHTML(
                (new ProductCardList(ProductType::MANAGEABLE_PRODUCT, array(ProductCard::EMPTY_PRODUCT_DATA), false))->close()->getHTML()
            )->close()->getHTML(),
            (new Row("justify-content-center"))->addHTML(
                (new BrowseButtons("prevSearch", "nextSearch", "col-10 col-lg-6 text-center"))->close()->getHTML()
            )->close()->getHTML()
        );
    }
}
