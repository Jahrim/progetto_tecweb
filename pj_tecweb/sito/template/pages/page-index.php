<?php
require_once dirname(__FILE__) . '/../components/main.php';
require_once dirname(__FILE__) . '/../components/advanced/research-bar.php';
require_once dirname(__FILE__) . '/../components/advanced/product-card.php';
require_once dirname(__FILE__) . '/../components/advanced/browse-buttons.php';

/**
 * This HTMLComponent represents the main content of the page index.html.
 */
class PageIndex extends Main {
    public function __construct($tp){
        parent::__construct();
        $this->addHTML(
            (new Row())->addHTML(
                (new ResearchBar("searchInput","Cerca un prodotto...","col-12"))->close()->getHTML()
            )->close()->getHTML(),
            (new Row("justify-content-center"))->addHTML(
                (new ProductCardList(ProductType::HOME_PRODUCT, array(ProductCard::EMPTY_PRODUCT_DATA), true))->close()->getHTML()
            )->close()->getHTML(),
            (new Row("justify-content-center"))->addHTML(
                (new BrowseButtons("prevSearch", "nextSearch", "col-10 col-lg-6 text-center"))->close()->getHTML()
            )->close()->getHTML()
        );
    }
}
?>