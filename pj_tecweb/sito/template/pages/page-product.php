<?php
require_once dirname(__FILE__) . '/../components/main.php';
require_once dirname(__FILE__) . '/../components/advanced/star-rating.php';

/**
 * This HTMLComponent represents the main content of the page product.html.
 */
class PageProduct extends Main
{
    public function __construct($tp)
    {
        $product = $tp['product'] ?? '';
        $productID = $product["productID"] ?? '';
        $vendorName = $tp['vendor']['name'];
        $canReview = $tp["canReview"] ?? true;
        $image = $product["image"] ?  UPLOAD_DIR . $product["image"] :  '';
        $hasSuccessfullReviewed = $tp["hasReviewed"] ?? '';
        $vendorPhone = isset($tp['vendor']['phone']) ? $tp['vendor']['phone'] : 'N/A';
        $vendorCompanyID = isset($tp['vendor']['companyID']) ? $tp['vendor']['companyID'] : 'N/A';
        $vendorCompanyIVA = isset($tp['vendor']['IVA']) ? $tp['vendor']['IVA'] : 'N/A';
        $vendorCompanyPEC = isset($tp['vendor']['companyPEC']) ? $tp['vendor']['companyPEC'] : 'N/A';
        $reviews = $tp['reviews'] ?? '';
        parent::__construct();
        $this->addHTML(<<<HTML
            <div class="row text-center justify-content-center">
                <header>
                    <div class="col-12">
                        <h2>{$product['name']}</h2>
                    </div>
                    <figure>
                        <img alt="immagine prodotto" src="{$image}" />
                    </figure>
                </header>
            </div>
            <div>
                <p>{$product['shortDescription']}</p>
                <p>{$product['longDescription']}</p>
                <p>Nome del venditore: {$vendorName}</p>
                <p>Telefono del venditore: {$vendorPhone}</p>
                <p>Codice dell'azienda: {$vendorCompanyID}</p>
                <p>IVA dell'azienda: {$vendorCompanyIVA}</p>
                <p>PEC aziendale: {$vendorCompanyPEC}</p>
                <div class="text-center">
                    <h3><strong>Prezzo: {$product['price']}€</strong></h3>
                </div>
            </div>
        HTML)->conditionalAddHTML(
            $canReview,
            (new StarRating([], StarRatingType::FORM_RATING, $productID))->close()->getHTML()
        )->conditionalAddHTML($hasSuccessfullReviewed, <<<HTML
            <div class="text-center text-success">{$hasSuccessfullReviewed}</div>
        HTML)->addHTML(
            (new StarRating(["rating" => $product["averageRating"] ?? ''], StarRatingType::FINAL_RATING))->close()->getHTML(),
            (new StarRatingList($reviews))->close()->getHTML()
        );
    }
}
