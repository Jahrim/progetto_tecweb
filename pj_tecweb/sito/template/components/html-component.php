<?php

/**
 * This class models an html component. 
 * Classes that extends this class must define the open-tag and the
 * closure-tag of this html component; they should also be set as
 * opened at creation. An HTMLComponent should expose its content by
 * extraction or by plotting it, only when closed.
 * This class uses a functional approach for invoking class methods.
 */
abstract class HTMLComponent {
    protected $content;
    protected $isModifiable;

    /** STATIC ----------------------------------------------------------------------- */
    /**
     * Returns the string representing the string $class2 appended at 
     * the end of the string $class1.
     */
    public static function appendClass($class1,$class2){
        $separator = (empty($class1) || empty($class2)) ? '' : ' ';
        return $class1.$separator.$class2;
    }


    /** NON-STATIC ------------------------------------------------------------------- */
    /**
     * Add $htmlString to the component if the $condition is met.
     * Returns this component.
     */
    public function conditionalAddHTML($condition, $htmlString){
        if ($condition) { $this->addHTML($htmlString); }
        return $this;
    }
    /**
     * Add all $htmlStrings to the component.
     * Returns this component.
     */
    public function addHTML(...$htmlStrings){
        if ($this->isModifiable()){ 
            foreach ($htmlStrings as $htmlString){
                $this->content .= $htmlString; 
            }
        }
        return $this;
    }
    /**
     * Plot the completed html of this component in the page.
     * It should be called only once the component is closed.
     */
    public function plotHTML() {
        echo $this->getHTML();
    }
    /**
     * Returns the completed html of this component in the page.
     * It should be called only once the component is closed.
     */
    public function getHTML() {
        return $this->content;
    }

    /**
     * Prevent further modification of this component.
     * Returns this component.
     */
    public function close(){
        $this->addHTML($this->getClosedTag());
        $this->isModifiable = false;
        return $this;
    }
    /**
     * Returns the closed tag of this component.
     */
    public abstract function getClosedTag();

    /**
     * Resets this component, allowing further modification of this component.
     * Returns this component.
     */
    public function open($attributes = array()){
        $this->content="";
        $this->isModifiable = true;
        return $this->addHTML($this->getOpenTag($attributes));
    }
    /**
     * Returns the opened tag of this component.
     */
    public abstract function getOpenTag($attributes = array());

    /**
     * Returns true if this component is still modifiable.
     */
    public function isModifiable(){
        return $this->isModifiable;
    }
    
}

/**
 * This class models an HTMLComponent without open-tag and closure-tag, granting
 * more freedom at the cost of more risk.
 */
class HTMLBuilder extends HTMLComponent {
    public function __construct(){
        $this->open();
    }
    public function getOpenTag($attribute = array()){
        return '';
    }
    public function getClosedTag(){
        return $this->getOpenTag();
    }
}
?>