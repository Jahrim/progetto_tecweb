<?php
require_once 'html-component.php';

/**
 * This HTMLComponent represents <body>...</body> in html.
 */
class Body extends HTMLComponent {
    
    public function __construct(){
        $this->open();
    }
    
    public function getOpenTag($attributes = array()){
        return <<<HTML
            <body>
        HTML;
    }
    public function getClosedTag(){
        return <<<HTML
            </body>
        HTML;
    }
}