<?php
require_once 'html-component.php';

/**
 * This HTMLComponent represents <main>...</main> in html.
 */
class Main extends HTMLComponent {
    
    public function __construct(){
        $this->open();
    }
    
    public function getOpenTag($attributes = array()){
        return <<<HTML
            <main>
        HTML;
    }
    public function getClosedTag(){
        return <<<HTML
            </main>
        HTML;
    }
}
?>