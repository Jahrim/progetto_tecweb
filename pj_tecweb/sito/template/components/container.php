<?php
require_once 'html-component.php';

/**
 * This HTMLComponent represents <div>...</div> in html.
 */
class Container extends HTMLComponent {
    public function __construct($class=''){
        $this->open(['class' => $class]);
    }
    
    public function getOpenTag($attributes = array()){
        $class = $attributes['class'];
        return <<<HTML
            <div class='{$class}'>
        HTML;
    }
    public function getClosedTag(){
        return <<<HTML
            </div>
        HTML;
    }
}

/**
 * This HTMLComponent represents a .row in html bootstrap.
 */
class Row extends Container {
    public function __construct($class=''){
        parent::__construct(HTMLComponent::appendClass('row',$class));
    }
}

/**
 * This HTMLComponent represents a .col in html bootstrap.
 */
class Col extends Container {
    /**
     * Builds a Col of the specified $class. If no .col-xx-xx is
     * specified inside $class, by default this will be a .col-12.
     */
    public function __construct($class=''){
        if (strpos($class, 'col') === false){
            $class = HTMLComponent::appendClass('col-12',$class);
        }
        parent::__construct($class);
    }
}