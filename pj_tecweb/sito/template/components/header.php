<?php
require_once 'html-component.php';

/**
 * This HTMLComponent represents <header>...</header> in html.
 * Its standard implementation is the main header of the page.
 */
class Header extends HTMLComponent {
    
    public function __construct($title){
        $logoutQuery = QueryParameter::queryGET(QueryParameter::ACTION, Action::LOGOUT);
        $this->open();
        $this->addHTML(<<<HTML
            <nav class="row align-items-center">
                <div class="col-5 px-0">
                    <a href="index.php"><img src="upload/logo.png" alt="shopzone" /></a>
                </div>
                <div class="col-7 px-0">
        HTML)->conditionalAddHTML( !UserHelper::isLoggedIn() , <<<HTML
                    <a class="btn btn-light" href="login.php" role="button">Login <em
                            class="bi bi-door-open"></em></a>
                    <a class="btn btn-light" href="signin.php" role="button">Sign in <em class="bi bi-pen"></em></a>
        HTML)->conditionalAddHTML( UserHelper::isUser(), (new HTMLBuilder())->addHTML(<<<HTML
                    <div class="dropdown" style="display: inline;">
                        <button class="btn btn-light dropdown-toggle" type="button" id="productMenuBtn"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Prodotti <em class="bi bi-cart"></em>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="productMenuBtn">
                            <a class="dropdown-item" href="cart.php">Carrello</a>
                            <a class="dropdown-item" href="notification.php">Notifiche</a>
                            <a class="dropdown-item" href="history.php">Cronologia</a>
                    HTML)->conditionalAddHTML( UserHelper::isVendor(), <<<HTML
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="sell.php">Gestisci prodotti</a>
                    HTML)->addHTML(<<<HTML
                        </div>
                    </div>
                    <div class="dropdown" style="display: inline;">
                        <button class="btn btn-light dropdown-toggle" type="button" id="profileMenuBtn"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Profilo <em class="bi bi-person-circle"></em>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="profileMenuBtn">
                            <a class="dropdown-item" href="profile.php">Impostazioni</a>
                            <a class="dropdown-item" href="index.php?{$logoutQuery}">Logout</a>
                        </div>
                    </div>
        HTML)->close()->getHTML())->addHTML(<<<HTML
                </div>
            </nav>
            <div class="row">
                <div class="col-12">
                    <h1>{$title}</h1>
                </div>
            </div>
        HTML);
    }
    
    public function getOpenTag($attributes = array()){
        return <<<HTML
            <header>
        HTML;
    }
    public function getClosedTag(){
        return <<<HTML
            </header>
        HTML;
    }
}
?>