<?php
require_once 'html-component.php';

/**
 * This HTMLComponent represents <!doctype html><html>...</html> in html.
 */
class HTML extends HTMLComponent {
    
    public function __construct($language = "it"){
        $this->open(['language' => $language]);
    }
    
    public function getOpenTag($attributes = array()){
        $language = $attributes['language'];
        return <<<HTML
            <!doctype html>
                <html lang='{$language}'>
        HTML;
    }
    public function getClosedTag(){
        return <<<HTML
            </html>
        HTML;
    }
}