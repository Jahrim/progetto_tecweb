<?php

require_once 'html-component.php';

/**
 * This HTMLComponent represents <head>...</head> in html.
 */
class Head extends HTMLComponent {
    public function __construct(){
        $this->open();
        $this->addHTML(<<<HTML
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        HTML);
    }

    /**
     * Imports the css sheets specified in the paths $urls.
     */
    public function importCSS($urls){
        if(isset($urls)){
            foreach($urls as $url){
                $this->addHTML(<<<HTML
                    <link type="text/css" rel="stylesheet" href="{$url}"/>
                HTML);
            }
        }
        return $this;
    }
    /**
     * Imports the icons specified in the paths $urls.
     */
    public function importIcons($urls){
        if(isset($urls)){
            foreach($urls as $url){
                $this->addHTML(<<<HTML
                    <link rel="stylesheet" href="{$url}"/>
                HTML);
            }
        }
        return $this;
    }
    /**
     * Imports the scripts specified in the paths $urls.
     */
    public function importScripts($urls){
        if(isset($urls)){
            foreach($urls as $url){
                $this->addHTML(<<<HTML
                    <script src="{$url}"></script>
                HTML);
            }
        }
        return $this;
    }
    /**
     * Sets the title of the page.
     */
    public function setTitle($title){
        return $this->addHTML(<<<HTML
                    <title>{$title}</title>
        HTML);
    }

    public function getOpenTag($attributes = array()){
        return <<<HTML
            <head>
        HTML;
    }
    public function getClosedTag(){
        return <<<HTML
            </head>
        HTML;
    }
}

?>