<?php
require_once 'html-component.php';

/**
 * This HTMLComponent represents <footer>...</footer> in html.
 * Its standard implementation is the main footer of the page.
 */
class Footer extends HTMLComponent {
    
    public function __construct(){
        $this->open();
        $this->addHTML(<<<HTML
                <nav class="row align-items-center">
                    <div class="col-4 px-0">
                        <a href="index.php">Condizioni d'uso</a>
                    </div>
                    <div class="col-4 px-0">
                        <a href="index.php">Privacy policy</a>
                    </div>
                    <div class="col-4 px-0">
                        <p>&copy; Shopzone copyright</p>
                    </div>
                </nav>
        HTML);
    }
    
    public function getOpenTag($attributes = array()){
        return <<<HTML
            <footer>
        HTML;
    }
    public function getClosedTag(){
        return <<<HTML
            </footer>
        HTML;
    }
}
?>