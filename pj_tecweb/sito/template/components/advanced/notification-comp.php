<?php
require_once dirname(__FILE__) . '/button.php';

/**
 * Build a row of a notification in the notification table.
 */
class Notification extends HTMLComponent
{
    const EMPTY_PRODUCT_DATA = array(
        "notificationID" => '',
        "subject" => '',
        "content" => ''
    );

    /**
     * Builds a notification with:
     * - the specified $notificationData
     */
    public function __construct($notificationData)
    {
        $notificationID = $notificationData["notificationID"];
        $object = $notificationData["subject"];
        $description = $notificationData["content"];
        $this->open(['id' => $notificationID]);
        $this->addHTML(<<<HTML
                    <td headers="object">{$object}</td>
                    <td headers="desc">{$description}</td>
                    <td headers="actions">      
        HTML)->addHTML(
            (new Button(
                ButtonType::LINK_BUTTON, 
                ButtonStyle::INTERACTIVE, 'Elimina', 'api/api-notification-list.php?' . QueryParameter::concatGET(
                                                                                            QueryParameter::queryGET(QueryParameter::ACTION,Action::DELETE_NOTIFICATION),
                                                                                            QueryParameter::queryGET(QueryParameter::NOTIFICATION_ID, $notificationID)
                                                                                        ),
                'pl-0 deleteNotificationBtn'))->close()->getHTML()
        )->addHTML(<<<HTML
                    </td>
                HTML);
    }

    public function getOpenTag($attributes = array())
    {
        $id = $attributes["id"];
        return <<<HTML
                <tr id='{$id}'> 
        HTML;
    }
    public function getClosedTag()
    {
        return <<<HTML
                </tr> 
        HTML;
    }
}

/**
 * Models a product list in this webpage.
 */
class NotificationList extends HTMLBuilder
{
    /**
     * Builds a notification list with the notifications passed argument.
     */
    public function __construct($notifications)
    {
        $this->open();
        $notificationDataList = array_slice($notifications, 0, C::MAX_NOTIFICATION_LIST_LENGTH);
        foreach ($notificationDataList as $notificationData) {
            $this->addHTML(
                (new Notification($notificationData))->close()->getHTML()
            );
        }
    }
}
