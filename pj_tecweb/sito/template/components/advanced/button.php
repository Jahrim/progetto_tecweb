<?php

require_once dirname(__FILE__) . '/../container.php';

/**
 * Enum of the button types available, each with its own structure html.
 */
abstract class ButtonType
{
    const ACTION_BUTTON = 1;
    const LINK_BUTTON = 2;
    const MODAL_BUTTON = 3;
}
/**
 * Enum of the button types available, each with its own style bootstrap.
 */
abstract class ButtonStyle
{
    const INTERACTIVE = 1;
    const VISUALIZATION = 2;
    const INTERACTIVE_BLOCK = 51;
    const VISUALIZATION_BLOCK = 52;
}
/**
 * Enum of the button istances available, used to make easier the creation
 * of standard buttons.
 */
abstract class ButtonInstance
{
    const ADD_TO_CART = 'btnAddToCart';
    const REMOVE_FROM_CART = 'btnRemoveFromCart';
    const UPDATE_PRODUCT = 'btnUpdateProduct';
    const DELETE_PRODUCT = 'btnDeleteProduct';
    const SHOW_PRODUCT = 'btnShowProduct';
    const RATE_PRODUCT = 'btnRateProduct';
    const END_PAYMENT = 'btnEndPayment';
}
/**
 * Enum of the button sizes available.
 */
abstract class ButtonSize
{
    const FULL = 'col-12 px-0';
    const HALF = 'col-5 px-0';
}

/**
 * Models a button in html bootstrap.
 */
class Button extends Col
{
    /**
     * Returns a button of the specified $buttonIstance.
     * The button is identified by a $marker which is:
     * - if buttonType::ACTION_BUTTON: the id attribute of the <button>;
     * - if buttonType::LINK_BUTTON: the href attribute of the <a>.
     * The $class refers to the container of the button.
     */
    public static function build($buttonIstance, $marker, $class = '')
    {
        HTMLComponent::appendClass($class, $buttonIstance);
        switch ($buttonIstance) {
            case ButtonInstance::END_PAYMENT:
                return new Button(ButtonType::LINK_BUTTON, ButtonStyle::INTERACTIVE_BLOCK, 'Concludi Pagamento', $marker, $class);
            case ButtonInstance::ADD_TO_CART:
                return new Button(ButtonType::LINK_BUTTON, ButtonStyle::INTERACTIVE_BLOCK, 'Aggiungi al carrello', $marker, $class);
            case ButtonInstance::REMOVE_FROM_CART:
                return new Button(ButtonType::LINK_BUTTON, ButtonStyle::INTERACTIVE_BLOCK, 'Rimuovi dal carrello', $marker, $class);
            case ButtonInstance::UPDATE_PRODUCT:
                return new Button(ButtonType::LINK_BUTTON, ButtonStyle::INTERACTIVE_BLOCK, 'Modifica', $marker, $class);
            case ButtonInstance::DELETE_PRODUCT:
                return new Button(ButtonType::LINK_BUTTON, ButtonStyle::INTERACTIVE_BLOCK, 'Elimina', $marker, $class);
            case ButtonInstance::SHOW_PRODUCT:
                return new Button(ButtonType::LINK_BUTTON, ButtonStyle::VISUALIZATION_BLOCK, 'Visualizza Prodotto', $marker, $class);
            case ButtonInstance::RATE_PRODUCT:
                return new Button(ButtonType::LINK_BUTTON, ButtonStyle::VISUALIZATION_BLOCK, 'Recensisci Prodotto', $marker, $class);
            default:
                die("Button.build : the specified button instance is not yet implemented.");
        }
    }
    /**
     * Build a button for a product of the specified $buttonType and $buttonStyle
     * containing the text $buttonText. The button is identified by a $marker which is:
     * - if buttonType::ACTION_BUTTON: the id attribute of the <button>;
     * - if buttonType::LINK_BUTTON: the href attribute of the <a>.
     * The $class refers to the container of the button.
     */
    public function __construct($buttonType, $buttonStyle, $buttonText, $marker, $class = '')
    {
        parent::__construct($class);
        $buttonClass = '';
        switch ($buttonStyle) {
            case ButtonStyle::INTERACTIVE:
                $buttonClass = 'btn btn-primary';
                break;
            case ButtonStyle::VISUALIZATION:
                $buttonClass = 'btn btn-success';
                break;
            case ButtonStyle::INTERACTIVE_BLOCK:
                $buttonClass = 'btn btn-primary btn-block';
                break;
            case ButtonStyle::VISUALIZATION_BLOCK:
                $buttonClass = 'btn btn-success btn-block';
                break;
            default:
                die("Button.new : the specified button style is not yet implemented.");
        }
        switch ($buttonType) {
            case ButtonType::ACTION_BUTTON:
                $this->addHTML(<<<HTML
                    <button id='{$marker}' class='{$buttonClass}' type="button" >{$buttonText}</button>
                HTML);
                break;
            case ButtonType::LINK_BUTTON:
                $this->addHTML(<<<HTML
                    <a href='{$marker}' class='{$buttonClass}' role="button">{$buttonText}</a>
                HTML);
                break;
            case ButtonType::MODAL_BUTTON:
                $this->addHTML(<<<HTML
                    <button data-toggle="modal" data-target='{$marker}' class='{$buttonClass}' type="button" >{$buttonText}</button>
                HTML);
                break;
            default:
                die("Button.new : the specified button type is not yet implemented.");
        }
    }
}
