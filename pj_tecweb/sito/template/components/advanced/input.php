<?php
require_once dirname(__FILE__) . '/button.php';

/**
 * Enum of the input styles available, used to design it.
 */
abstract class InputStyle
{
    const LOGGED_DEFAULT = array('form-control-plaintext', 'col-12 col-lg-9', 'col-12 col-lg-2 col-form-label');
    const LOGGED_WITH_BUTTON = array('form-control-plaintext', 'col-6 col-lg-3', 'col-12 col-lg-2 col-form-label');
    const NOT_LOGGED = array('form-control', 'col-8', 'col-3 col-form-label');
    const STAR_RATING = array('rating-input', '', 'rating-label');
    const MODAL = array('form-control', 'col-9', 'col-2 col-form-label');
    const FILE = array('form-control-plaintext', 'col-8', 'col-3 col-form-label');
}
/**
 * Build an input component.
 */
class Input extends Container
{
    /**
     * Build an input with:
     * @param string $id the id of the element
     * @param string $text the text to display for a non textarea element
     * @param string $inputType the type attribute of the element
     * @param InputStyle $inputStyle the style to design this element
     * @param string $placeholder the text to display for a textarea element, the placeholder otherwise.
     * @param string $modalId the id of modal opened by the element.
     * @param string $buttonText the text of the button that open the modal.
     */
    public function __construct($id, $text, $inputType, $inputStyle, $placeholder = '', $modalId = '', $buttonText = '')
    {
        switch ($inputType) {
            case "text":
            case "tel":
            case "password":
            case "email":
            case "number":
                $blockClass = "form-group row justify-content-between";
                parent::__construct($blockClass);
                $disabled = ($inputStyle != InputStyle::NOT_LOGGED && $inputStyle != InputStyle::MODAL) ? "disabled" : '';
                $this->addHTML(<<<HTML
                        <label for='{$id}' class='{$inputStyle[2]}'>{$text}:</label>
                        <div class='{$inputStyle[1]}'>
                            <input type='{$inputType}' id='{$id}' name='{$id}' class='{$inputStyle[0]}' placeholder='{$placeholder}' {$disabled}/>
                        </div>
                HTML)->conditionalAddHTML(
                    $modalId != '',
                    (new Button(ButtonType::MODAL_BUTTON, ButtonStyle::INTERACTIVE_BLOCK, $buttonText, $modalId, "col-6 col-lg-5"))->close()->getHTML()
                );
                break;
            case "submit":
                $blockClass = "row";
                parent::__construct($blockClass);
                $this->addHTML(<<<HTML
                            <div class="col-12">
                                <input id='{$id}' type="submit" name='{$id}' value='{$text}' class="btn btn-primary btn-block" />
                            </div>
                HTML);
                break;
            case "hidden":
                $blockClass = "row";
                parent::__construct($blockClass);
                $this->addHTML(<<<HTML
                                <div class="col-12">
                                    <input id='{$id}' type="hidden" name='{$id}' value='{$text}' />
                                </div>
                    HTML);
                break;
            case "file":
                $blockClass = "form-group row justify-content-between";
                parent::__construct($blockClass);
                $this->addHTML(<<<HTML
                            <label for='{$id}' class='{$inputStyle[2]}'>{$text}:</label>
                            <div class='{$inputStyle[1]}'>
                                <input type='{$inputType}' id='{$id}' name='{$id}' accept=".png,.jpeg,.jpg"/>
                            </div>
                HTML);
                break;
            case "textarea":
                parent::__construct();
                $this->addHTML(<<<HTML
                        <div class="form-group">
                            <label for='{$id}'>{$text}</label>
                            <textarea id='{$id}' rows="3" name='{$id}' class="form-control">{$placeholder}</textarea>
                        </div>
                HTML);
                break;
            default:
                die("Input.new : the specified input type is not yet implemented.");
        }
    }
}
