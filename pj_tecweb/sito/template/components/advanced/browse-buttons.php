<?php
require_once dirname(__FILE__) . '/../container.php';

/**
 * Models the two browse buttons at the bottom of the product list in this webpage.
 */
class BrowseButtons extends Col {

    /**
     * Builds the two browse buttons giving:
     * - the id $idPrev to the button that browses the list backward
     * - the id $idNext to the button that browses the list forward
     * - the class $class to the container of these buttons.
     */
    public function __construct($idPrev, $idNext, $class=''){
        parent::__construct($class);
        $this->addHTML(<<<HTML
            <button id="{$idPrev}" type="button" class="btn btn-outline-secondary"><em class="bi bi-arrow-left"></em> Indietro</button>
            <button id="{$idNext}" type="button" class="btn btn-outline-secondary">Avanti <em class="bi bi-arrow-right"></em></button>
        HTML);
    }
}
