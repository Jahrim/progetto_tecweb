<?php
require_once dirname(__FILE__) . '/../html-component.php';
require_once dirname(__FILE__) . '/../container.php';

/**
 * Models a research bar in this webpage.
 */
class ResearchBar extends Col {
    /**
     * Builds a ResearchBar with:
     * - the specified $id : to identify the search input.
     * - the specified $placeholder : that will be written in the bar.
     * - the specified $class : that can be used to resize the bar or else.
     */
    public function __construct($id, $placeholder, $class=''){
        parent::__construct(HTMLComponent::appendClass($class,"form-group"));
        $buttonID = $id . 'Btn';
        $this->addHTML(<<<HTML
                <label for="searchInput">Barra di ricerca</label>
                <div class="input-group">
                    <input id='{$id}' type="search" class="form-control" placeholder='{$placeholder}' aria-label='{$placeholder}'/>
                    <div class="input-group-append">
                        <button id='{$buttonID}' class="btn btn-outline-secondary" type="button">Cerca <em class="bi bi-search"></em></button>
                    </div>
                </div>
        HTML);
    }
}
?>
    