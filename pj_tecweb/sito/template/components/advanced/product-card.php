<?php
require_once dirname(__FILE__) . '/../html-component.php';
require_once dirname(__FILE__) . '/../footer.php';
require_once dirname(__FILE__) . '/button.php';

/**
 * Enum of the product types that distiguish the product structure and style
 * in different pages.
 */
abstract class ProductType {
    const HOME_PRODUCT = 1;
    const CART_PRODUCT = 2;
    const HISTORY_PRODUCT = 3;
    const MANAGEABLE_PRODUCT = 4;
}
/**
 * Enum of the product sizes available.
 */
abstract class ProductSize {
    const EXTENDABLE = array('col-11 col-lg-2','col-3 col-lg-11 pr-0 pr-lg-3','col-8 col-lg-11');
    const FIXED = array('col-11','col-3 pr-0','col-8');
}

/**
 * Models a product in this website.
 */
class ProductCard extends HTMLComponent {
    const EMPTY_PRODUCT_DATA = array(
        "productID" => '',
        "name" => '',
        "image" => '',
        "shortDescription" => '',
        'price' => 0
    );

    /**
     * Builds a product of:
     * - the specified $productType: indicating what html components this product contains.
     * - the specified $productData: ['productID']        => the id of this product
     *                               ['name']             => the name of this product
     *                               ['image']            => the image of this product
     *                               ['shortDescription'] => the short description of this product
     *                               ['price']            => the price of this product
     * A product that is $extendable will change style on larger screens.
     */
    public function __construct($productType, $productData, $extendable = false){
        $articleClass = ($extendable) ? ProductSize::EXTENDABLE[0] : ProductSize::FIXED[0];
        $asideClass = ($extendable) ? ProductSize::EXTENDABLE[1] : ProductSize::FIXED[1];
        $divClass = ($extendable) ? ProductSize::EXTENDABLE[2] : ProductSize::FIXED[2];

        $productID = $productData["productID"];
        $productName = $productData["name"];
        $productImage = UPLOAD_DIR . $productData["image"];
        $shortDescription = $productData["shortDescription"];
        $price = $productData["price"];

        $this->open(['class' => $articleClass]);
        $this->addHTML(<<<HTML
            <div class="row align-items-center justify-content-center">
                <aside class='{$asideClass}' aria-labelledby='{$productID}'>
                    <img src='{$productImage}' alt='{$productName}' />
                </aside>
                <div class='{$divClass}'>
                    <header>
                        <h2 id='{$productID}'>{$productName}</h2>
                    </header>
                    <p>{$shortDescription}</p>
                    <p>{$price}€</p>
        HTML)->addHTML((new ProductFooter($productID, $productType))->close()->getHTML())->addHTML(<<<HTML
                </div>
            </div>
        HTML);
    }

    public function getOpenTag($attributes = array()){
        $class = $attributes['class'];
        return <<<HTML
            <article class='{$class}'>
        HTML;
    }
    public function getClosedTag(){
        return <<<HTML
            </article>
        HTML;
    }
}

/**
 * Models a product list in this webpage.
 */
class ProductCardList extends HTMLBuilder {
    /**
     * Builds a product list containing at max 5 product of:
     * - the specified $productType: indicating what html components this product contains.
     * - the specified $productListData[i]: ['productID']        => the id of this product
     *                                      ['name']             => the name of this product
     *                                      ['image']            => the image of this product
     *                                      ['shortDescription'] => the short description of this product
     *                                      ['price']            => the price of this product
     * A product that is $extendable will change style on larger screens.
     */
    public function __construct($productType, $productListData, $extendable){
        $this->open();
        $productListData = array_slice($productListData, 0, C::MAX_PRODUCT_LIST_LENGTH);
        foreach($productListData as $productData){
            $this->addHTML(
                (new ProductCard($productType, $productData, $extendable))->close()->getHTML()
            );
        }
    }
}

/**
 * Models the footer of a product card in this webpage.
 */
class ProductFooter extends Footer {
    /**
     * Builds a footer of the specified:
     * - $productID : indicating what is the id of the product which contains this footer.
     * - $productType : indicating what buttons are contained in the footer.
     */
    public function __construct($productID, $productType){
        $this->open();
        $this->addHTML(<<<HTML
            <div class="row justify-content-between">
        HTML);
        switch($productType){

            case ProductType::HOME_PRODUCT:
                $this->conditionalAddHTML(UserHelper::isLoggedIn(),
                    Button::build(
                        ButtonInstance::ADD_TO_CART,
                        'api/api-product-list.php?' . QueryParameter::concatGET(
                                                        QueryParameter::queryGET(QueryParameter::ACTION, Action::ADD_TO_CART),
                                                        QueryParameter::queryGET(QueryParameter::PRODUCT_ID, $productID)
                                                      ), 
                        HTMLComponent::appendClass(ButtonSize::FULL, ButtonInstance::ADD_TO_CART)
                    )->close()->getHTML()
                )->addHTML(
                    Button::build(
                        ButtonInstance::SHOW_PRODUCT, 
                        'product.php?' . QueryParameter::queryGET(QueryParameter::PRODUCT_ID, $productID), 
                        HTMLComponent::appendClass(ButtonSize::FULL, ButtonInstance::SHOW_PRODUCT)
                    )->close()->getHTML()
                );
                break;

            case ProductType::CART_PRODUCT:
                $this->addHTML(
                    Button::build(
                        ButtonInstance::REMOVE_FROM_CART, 
                        'api/api-product-list.php?' . QueryParameter::concatGET(
                                                        QueryParameter::queryGET(QueryParameter::ACTION, Action::REMOVE_FROM_CART),
                                                        QueryParameter::queryGET(QueryParameter::PRODUCT_ID, $productID)
                                                      ), 
                        HTMLComponent::appendClass(ButtonSize::FULL, ButtonInstance::REMOVE_FROM_CART)
                    )->close()->getHTML()
                )->addHTML(
                    Button::build(
                        ButtonInstance::SHOW_PRODUCT, 
                        'product.php?' . QueryParameter::queryGET(QueryParameter::PRODUCT_ID, $productID), 
                        HTMLComponent::appendClass(ButtonSize::FULL, ButtonInstance::SHOW_PRODUCT)
                    )->close()->getHTML()
                );
                break;

            case ProductType::HISTORY_PRODUCT:
                $this->addHTML(
                    Button::build(
                        ButtonInstance::ADD_TO_CART, 
                        'api/api-product-list.php?' . QueryParameter::concatGET(
                                                        QueryParameter::queryGET(QueryParameter::ACTION, Action::ADD_TO_CART),
                                                        QueryParameter::queryGET(QueryParameter::PRODUCT_ID, $productID)
                                                      ), 
                        HTMLComponent::appendClass(ButtonSize::FULL, ButtonInstance::ADD_TO_CART)
                    )->close()->getHTML()
                )->addHTML(
                    Button::build(
                        ButtonInstance::SHOW_PRODUCT, 
                        'product.php?' . QueryParameter::queryGET(QueryParameter::PRODUCT_ID, $productID), 
                        HTMLComponent::appendClass(ButtonSize::HALF, ButtonInstance::SHOW_PRODUCT)
                    )->close()->getHTML()
                )->addHTML(
                    Button::build(
                        ButtonInstance::RATE_PRODUCT, 
                        'product.php?' . QueryParameter::queryGET(QueryParameter::PRODUCT_ID, $productID), 
                        HTMLComponent::appendClass(ButtonSize::HALF, ButtonInstance::RATE_PRODUCT)
                    )->close()->getHTML()
                );
                break;

            case ProductType::MANAGEABLE_PRODUCT:
                $this->addHTML(
                    Button::build(ButtonInstance::UPDATE_PRODUCT, 'manage-product.php?' . QueryParameter::queryGET(QueryParameter::PRODUCT_ID, $productID), HTMLComponent::appendClass(ButtonSize::HALF, ButtonInstance::UPDATE_PRODUCT))->close()->getHTML()
                )->addHTML(
                    Button::build(
                        ButtonInstance::DELETE_PRODUCT, 
                        'api/api-product-list.php?' . QueryParameter::concatGET(
                                                        QueryParameter::queryGET(QueryParameter::ACTION, Action::DELETE_PRODUCT),
                                                        QueryParameter::queryGET(QueryParameter::PRODUCT_ID, $productID)
                                                      ), 
                        HTMLComponent::appendClass(ButtonSize::HALF, ButtonInstance::DELETE_PRODUCT))->close()->getHTML()
                )->addHTML(
                    Button::build(ButtonInstance::SHOW_PRODUCT, 'product.php?' . QueryParameter::queryGET(QueryParameter::PRODUCT_ID, $productID), HTMLComponent::appendClass(ButtonSize::FULL, ButtonInstance::SHOW_PRODUCT))->close()->getHTML()
                );
                break;

            default: die("ProductFooter.new : the specified product type is not yet implemented.");
        }
        $this->addHTML(<<<HTML
            </div>
        HTML);
    }
}
?>