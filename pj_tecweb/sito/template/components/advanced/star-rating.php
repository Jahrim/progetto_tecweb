<?php
require_once dirname(__FILE__) . '/input.php';

/**
 * Enum of the star rating type available, used to design it.
 */
abstract class StarRatingType
{
    const FINAL_RATING = "final";
    const FORM_RATING = "form";
    const USER_RATING = "user";
}

/**
 * Build a star rating component.
 */
class StarRating extends Container
{
    /**
     * Build a star rating with:
     * @param mixed starRatingData the data of the elment
     * @param StarRatingType starRatingType the style to design its html structure.
     */
    public function __construct($starRatingData, $starRatingType, $productID = '')
    {
        $user = $starRatingData['name'] ?? '';
        $rating = $starRatingData["rating"] ?? '';
        $checkedFiveStar = ($rating >= 5 && $starRatingType != StarRatingType::FORM_RATING) ? "text-warning" : '';
        $checkedFourStar = ($rating >= 4 && $starRatingType != StarRatingType::FORM_RATING) ? "text-warning" : '';
        $checkedThreeStar = ($rating >= 3 && $starRatingType != StarRatingType::FORM_RATING) ? "text-warning" : '';
        $checkedTwoStar = ($rating >= 2 && $starRatingType != StarRatingType::FORM_RATING) ? "text-warning" : '';
        $checkedOneStar = ($rating >= 1 && $starRatingType != StarRatingType::FORM_RATING) ? "text-warning" : '';
        $description = $starRatingData["description"] ?? '';
        $blockClass = "row";
        $disabled = $starRatingType != StarRatingType::FORM_RATING ? "disabled" : '';
        parent::__construct($blockClass);
        $this->buildHeader($starRatingType, $user);
        $this->addHTML(<<<HTML
                    <input id='{$starRatingType}Star5{$user}' type="radio" name="rating" value="5" class="rating-input"
                    {$disabled} />
                    <label for='{$starRatingType}Star5{$user}' class="rating-label {$checkedFiveStar}" aria-label="cinque stelle"
                        title="cinque stelle">&#9733;</label>
                    <input id='{$starRatingType}Star4{$user}' type="radio" name="rating" value="4" class="rating-input"
                    {$disabled} />
                    <label for='{$starRatingType}Star4{$user}' class="rating-label {$checkedFourStar}" aria-label="quattro stelle"
                        title="quattro stelle">&#9733;</label>
                    <input id='{$starRatingType}Star3{$user}' type="radio" name="rating" value="3" class="rating-input"
                    {$disabled} />
                    <label for='{$starRatingType}Star3{$user}' class="rating-label {$checkedThreeStar}" aria-label="tre stelle"
                        title="tre stelle">&#9733;</label>
                    <input id='{$starRatingType}Star2{$user}' type="radio" name="rating" value="2" class="rating-input"
                    {$disabled} />
                    <label for='{$starRatingType}Star2{$user}' class="rating-label {$checkedTwoStar}" aria-label="due stelle"
                        title="due stelle">&#9733;</label>
                    <input id='{$starRatingType}Star1{$user}' type="radio" name="rating" value="1" class="rating-input"
                    {$disabled} />
                    <label for='{$starRatingType}Star1{$user}' class="rating-label {$checkedOneStar}" aria-label="una stella"
                        title="una stella">&#9733;</label>
        HTML);
        $this->buildFooter($starRatingType, $productID, $description);
    }

    public function buildHeader($starRatingType, $user)
    {
        switch ($starRatingType) {
            case StarRatingType::FINAL_RATING:
                $this->addHTML(<<<HTML
                                    <div class="col-4 col-md-6">
                                        <h3>Valutazione Complessiva</h3>
                                    </div>
                                    <div class="col-8 col-md-6">
                                        <div class="rating">
                                            <fieldset>
                                                <legend></legend>
                                                    <div>
                HTML);
                break;
            case StarRatingType::FORM_RATING:
                $this->addHTML(<<<HTML
                                    <article class="col-12">
                                        <form id="ratingForm" action="#" method="POST">
                                            <h4>Lascia qui la tua Recensione</h4>
                                            <div class="row">
                                                <div class="col-4">
                                                    <h5>Valutazione</h5>
                                                </div>
                                                <div class="col-8">
                                                    <div class="rating">
                                                        <fieldset>
                                                            <legend></legend>
                                                                <div dir="rtl">
                    HTML);
                break;
            case StarRatingType::USER_RATING:
                $this->addHTML(<<<HTML
                                    <article class="col-12">
                                        <h4>Recensione di {$user}</h4>
                                        <div class="row">
                                            <div class="col-4">
                                                <h5>Valutazione</h5>
                                            </div>
                                            <div class="col-8">
                                                <div class="rating">
                                                    <fieldset>
                                                        <legend></legend>
                                                            <div dir="rtl">
                        HTML);
                break;
            default:
                die("Star Rating.new : the specified star rating type is not yet implemented.");
        }
    }

    public function buildFooter($starRatingType, $productID, $description)
    {
        switch ($starRatingType) {
            case StarRatingType::FINAL_RATING:
                $this->addHTML(<<<HTML
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                HTML);
                break;
            case StarRatingType::FORM_RATING:
                $this->addHTML(<<<HTML
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                HTML)->addHTML(
                    (new Input("personalReview", "Recensione:", "textarea", InputStyle::LOGGED_DEFAULT))->close()->getHTML(),
                    (new Input("submit", "Invia Recensione", "submit", InputStyle::LOGGED_DEFAULT))->close()->getHTML(),
                    (new Input(QueryParameter::PRODUCT_ID, $productID, "hidden", InputStyle::LOGGED_DEFAULT))->close()->getHTML()
                )->addHTML(<<<HTML
                                </form>
                            </article>
                    HTML);
                break;
            case StarRatingType::USER_RATING:
                $this->addHTML(<<<HTML
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <p>{$description}</p>
                                </article>
                        HTML);
                break;
            default:
                die("Star Rating.new : the specified star rating type is not yet implemented.");
        }
    }
}

/**
 * Models a star rating list in this webpage.
 */
class StarRatingList extends HTMLBuilder
{
    /**
     * Builds a user star rating list with the star ratings passed argument.
     * @param mixed $starRatings the array containing all the user star rating of a specific product.
     */
    public function __construct($starRatings)
    {
        $this->open();
        $starRatingList = array_slice($starRatings, 0);
        foreach ($starRatingList as $starRatingData) {
            $this->addHTML(
                (new StarRating($starRatingData, StarRatingType::USER_RATING))->close()->getHTML()
            );
        }
    }
}
