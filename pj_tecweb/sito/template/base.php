
<?php
    require_once 'components/html.php';
    require_once 'components/head.php';
    require_once 'components/body.php';
    require_once 'components/container.php';
    require_once 'components/header.php';
    require_once 'components/footer.php';

    (new HTML("it"))->addHTML(
        (new Head())->importCSS($tp["css"])->importIcons($tp["icon"])->importScripts($tp["js"])->setTitle($tp["meta-title"])->close()->getHTML(),
        (new Body())->addHTML(
            (new Container("container-fluid"))->addHTML(
                (new Header($tp["title"]))->close()->getHTML(),
                PageLoader::load($tp)->close()->getHTML(),
                (new Footer())->close()->getHTML()
            )->close()->getHTML()
        )->close()->getHTML()
    )->close()->plotHTML();

?>