<?php
require_once 'init.php';

if (UserHelper::isLoggedIn()) {
    /* IMPORT ---------------------------------------------------------------------------*/
    $tp["css"] = array(
        "external/bootstrap-v4.5.3.css",
        "css/components/comp-header-footer.css",
        "css/pages/page-profile.css"
    );
    $tp["icon"] = array(
        "https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css"
    );
    $tp["js"] = array(
        "external/jquery-v3.5.1.js",
        "external/bootstrap.bundle.v4.5.3.js",
        "js/page-profile.js"
    );

    /* PAGE DATA -------------------------------------------------------------------------*/
    $tp["errorWrongPassword"] = null;
    $tp["updateUserDataSuccess"] = null;
    $tp["unlockVendorLicenseSuccess"] = null;
    $tp["updateUserPasswordSuccess"] = null;
    /* check if user call the form to upgrade his data */
    if (
        isset($_POST["email"]) && isset($_POST["phone"])
        && isset($_POST["address"]) && isset($_POST["creditCard"])
    ) {
        $databaseHelper->updateUserData($_SESSION["userID"], $_POST["email"], $_POST["phone"], $_POST["address"], $_POST["creditCard"]);
        $userData = $databaseHelper->getUserByID($_SESSION['userID']);
        UserHelper::saveSessionUserData($userData);
        $tp["updateUserDataSuccess"] = "I dati sono stati aggiornati con successo!";
    }

    /* check if user call the form to register him as a vendor */
    if (isset($_POST["modalCompanyId"]) && isset($_POST["modalIva"]) && isset($_POST["modalPec"])) {
        $databaseHelper->unlockVendorLicense($_SESSION["userID"], $_POST["modalCompanyId"], $_POST["modalIva"], $_POST["modalPec"]);
        $userData = $databaseHelper->getUserByID($_SESSION['userID']);
        UserHelper::saveSessionUserData($userData);
        $databaseHelper->insertNotification($_SESSION["userID"], "Licenza Ottenuta", "Registrazione come negoziante avvenuta.");
        $tp["unlockVendorLicenseSuccess"] = "Licenza da negoziante ottenuta con successo!";
    }

    /* check if user call the form to upgrade his password */
    if (isset($_POST["modalPassword"]) && isset($_POST["modalPasswordConfirm"]) && isset($_POST["modalLastPassword"])) {
        if (count($databaseHelper->checkUserPassword($_SESSION["userID"], $_POST["modalLastPassword"])) != 0) {
            $databaseHelper->updateUserPassword($_SESSION["userID"], $_POST["modalPassword"]);
            $tp["updateUserPasswordSuccess"] = "Password modificata con successo!";
        } else {
            $tp["errorWrongPassword"] = "La vecchia password non corrisponde con quella dell'utente.";
        }
    }

    $tp["meta-title"] = "Shopzone - Profilo";
    $tp["title"] = "Profilo";
    $tp["page"] = Page::PROFILE;

    require 'template/base.php';
} else {
    header("location: index.php");
}
