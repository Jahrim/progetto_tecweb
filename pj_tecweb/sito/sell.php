<?php
require_once 'init.php';

if (UserHelper::isVendor()) {
    /* IMPORT ---------------------------------------------------------------------------*/
    $tp["css"] = array(
        "external/bootstrap-v4.5.3.css",
        "css/components/comp-header-footer.css",
        "css/components/comp-product-list.css",
        "css/components/comp-searchbar.css",
        "css/components/comp-browse-buttons.css"
    );
    $tp["icon"] = array(
        "https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css"
    );
    $tp["js"] = array(
        "external/jquery-v3.5.1.js",
        "external/bootstrap.bundle.v4.5.3.js",
        "js/util.js",
        "js/product-list.js",
        "js/browse-buttons.js",
        "js/search-bar.js"
    );

    /* PAGE DATA -------------------------------------------------------------------------*/
    if (!isset($_SESSION["phone"]) || !isset($_SESSION["creditCard"]) || $_SESSION["phone"] == "" || $_SESSION["creditCard"] == "") {
        $tp["missingData"] = "Prima di aggiungere prodotti è necessario aver inserito il proprio numero di telefono e la propria carta di credito (Profilo -> Impostazioni).";
    }
    if (isset($_GET[QueryParameter::STATUS])) {
        $status = $_GET[QueryParameter::STATUS];
        if ($status == Status::INSERT_SUCCESS) {
            $tp["successAction"] = "Il prodotto è stato registrato con successo.";
        } else if ($status == Status::UPDATE_SUCCESS) {
            $tp["successAction"] = "Il prodotto è stato aggiornato con successo.";
        }
    }
    $tp["meta-title"] = "Shopzone - I tuoi Prodotti";
    $tp["title"] = "I tuoi Prodotti";
    $tp["page"] = Page::SELL;

    require 'template/base.php';
} else {
    header("location: index.php");
}
