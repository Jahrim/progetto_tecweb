<?php

/**
 * Models a query executor.
 */
class DatabaseHelper
{
    const DEBUG = FALSE;

    const ENTITIES = array("User", "Company", "Product", "Notification");
    const RELATIONS = array("Agent", "Rating", "Purchase");
    const IDS = array("userID", "companyID", "productID", "notificationID");

    const USER_ROLE = 'user';
    const VENDOR_ROLE = 'vendor';
    const ADMIN_ROLE = 'admin';

    private $db;

    public function __construct($servername, $username, $password, $dbname, $port)
    {
        $this->db = new mysqli($servername, $username, $password, $dbname, $port);
        if ($this->db->connect_error) {
            die("DatabaseHelper.__construct: Connection failed: " . $this->db->connect_error);
        }
    }

    public function getProducts($productIDs = array(), $excludeCart = true, $excludeOwn = true)
    {
        $availability = (DatabaseHelper::DEBUG) ? 'false' : 'true';
        if (empty($productIDs)) {
            $stmt = $this->db->prepare("SELECT productID, name, image, shortDescription, longDescription, price, availability, averageRating, vendorID
                                        FROM Product
                                        WHERE availability = $availability
                                        ORDER BY productID ASC");
        } else {
            $values = $this->prepareInClauseVariableText($productIDs);
            $stmt = $this->db->prepare("SELECT productID, name, image, shortDescription, longDescription, price, availability, averageRating, vendorID
                                        FROM Product
                                        WHERE productID IN ($values) AND availability = $availability
                                        ORDER BY productID ASC");
            $stmt->bind_param(str_repeat("s", count($productIDs)), ...$productIDs);
        }
        $result = ($excludeOwn) ? $this->getResultExcludingOwn($this->retrieveResult($stmt)) : $this->retrieveResult($stmt);
        return ($excludeCart) ? $this->getResultExcludingCart($result) : $result;
    }
    public function getHistoryProducts($customerID)
    {
        $availability = (DatabaseHelper::DEBUG) ? 'false' : 'true';
        $stmt = $this->db->prepare("SELECT pr.productID, name, image, shortDescription, longDescription, price, availability, averageRating, vendorID
                                    FROM Purchase pu, Product pr
                                    WHERE pu.productID = pr.productID AND customerID = ? AND availability = $availability
                                    ORDER BY pr.productID ASC");
        $stmt->bind_param('s', $customerID);
        return $this->getResultExcludingCart($this->getResultExcludingOwn($this->retrieveResult($stmt)));
    }
    public function getVendorProducts($vendorID)
    {
        $availability = (DatabaseHelper::DEBUG) ? 'false' : 'true';
        $stmt = $this->db->prepare("SELECT productID, name, image, shortDescription, longDescription, price, availability, averageRating, vendorID
                                    FROM Product
                                    WHERE vendorID = ? AND availability = $availability
                                    ORDER BY productID ASC");
        $stmt->bind_param('s', $vendorID);
        return $this->retrieveResult($stmt);
    }

    public function insertProduct($name, $image, $shortDescription, $longDescription, $price, $vendorID)
    {
        $stmt = $this->db->prepare("INSERT INTO Product (productID, name, image, shortDescription, longDescription, price, availability, averageRating, vendorID)
                                    VALUES (?,?,?,?,?,?,'1','0',?)");
        $productID = $this->getNextID("Product", "productID");
        $stmt->bind_param("sssssis", $productID, $name, $image, $shortDescription, $longDescription, $price, $vendorID);
        $stmt->execute();
        return $stmt->insert_id;
    }

    public function updateProduct($productID, $name, $image, $shortDescription, $longDescription, $price)
    {
        if ($image != null && $image != '') {
            $stmt = $this->db->prepare("UPDATE Product SET name = ?, image = ?, shortDescription = ?, longDescription = ?, price = ? WHERE productID = ?");
            $stmt->bind_param('ssssis', $name, $image, $shortDescription, $longDescription, $price, $productID);
        } else {
            $stmt = $this->db->prepare("UPDATE Product SET name = ?, shortDescription = ?, longDescription = ?, price = ? WHERE productID = ?");
            $stmt->bind_param('sssis', $name, $shortDescription, $longDescription, $price, $productID);
        }
        $stmt->execute();
    }

    public function reviewProduct($productID, $userID, $rating, $description)
    {
        $stmt = $this->db->prepare("INSERT INTO Rating (productID, customerID, rating, description) VALUES (?,?,?,?)");
        $stmt->bind_param("ssss", $productID, $userID, $rating, $description);
        $stmt->execute();
        //udpate overall rating of product
        $stmt = $this->db->prepare("SELECT AVG(rating) AS overallRating FROM Rating WHERE productID = ? GROUP BY productID");
        $stmt->bind_param("s", $productID);
        $overallRating = round($this->retrieveResult($stmt)[0]["overallRating"]);
        $this->updateAverageRatingProduct($productID, $overallRating);
    }

    public function updateAverageRatingProduct($productID, $overallRating)
    {
        $stmt = $this->db->prepare("UPDATE Product SET averageRating=? WHERE productID=?");
        $stmt->bind_param("is", $overallRating, $productID);
        $stmt->execute();
    }

    public function updateProductAvailability($productID, $availability)
    {
        $stmt = $this->db->prepare("UPDATE Product
                                    SET availability=?
                                    WHERE productID=?");
        $stmt->bind_param("is", $availability, $productID);
        $stmt->execute();
        return $this->db->affected_rows >= 0;
    }

    public function registerUser($name, $email, $password)
    {
        $stmt = $this->db->prepare("INSERT INTO User (userID, role, name, email, password, phone, address, creditCard) VALUES (?,'user',?,?,?,NULL,NULL,NULL)");
        $userID = $this->getNextID("User", "userID");
        $stmt->bind_param("ssss", $userID, $name, $email, $password);
        $stmt->execute();
        return $stmt->insert_id;
    }

    public function unlockVendorLicense($userID, $companyID, $iva, $companyPEC)
    {
        $stmt = $this->db->prepare("SELECT companyID FROM Company WHERE companyID = ?");
        $stmt->bind_param("s", $companyID);
        if (empty($this->retrieveResult($stmt))) {
            $this->insertCompany($companyID, $iva, $companyPEC);
        }
        $this->updateUserRole($userID, DatabaseHelper::VENDOR_ROLE);
        $this->insertAgent($companyID, $userID);
    }

    public function insertCompany($companyID, $iva, $companyPEC)
    {
        $stmt = $this->db->prepare("INSERT INTO Company (companyID, iva, companyPEC) VALUES (?,?,?)");
        $stmt->bind_param("sss", $companyID, $iva, $companyPEC);
        $stmt->execute();
    }

    public function insertAgent($companyID, $userID)
    {
        $stmt = $this->db->prepare("INSERT INTO Agent (companyID, userID) VALUES (?,?)");
        $stmt->bind_param("ss", $companyID, $userID);
        $stmt->execute();
    }

    public function updateUserPassword($userID, $password)
    {
        $stmt = $this->db->prepare("UPDATE User SET password = ? WHERE userID = ?");
        $stmt->bind_param('ss', $password, $userID);
        $stmt->execute();
        return $this->db->affected_rows >= 0;
    }

    public function updateUserRole($userID, $role)
    {
        $stmt = $this->db->prepare("UPDATE User SET role = ? WHERE userID = ?");
        $stmt->bind_param('ss', $role, $userID);
        $stmt->execute();
        return $this->db->affected_rows >= 0;
    }

    public function updateUserData($userID, $email, $phone, $address, $creditCard)
    {
        $stmt = $this->db->prepare("UPDATE User SET email = ?, phone = ?, address = ?, creditCard = ? WHERE userID = ?");
        $stmt->bind_param('sssss', $email, $phone, $address, $creditCard, $userID);
        $stmt->execute();
        return $this->db->affected_rows >= 0;
    }

    public function isEmailAvailable($email)
    {
        $stmt = $this->db->prepare("SELECT userID FROM User WHERE email=?");
        $stmt->bind_param('s', $email);
        return empty($this->retrieveResult($stmt));
    }

    public function checkUserPassword($userID, $password)
    {
        $stmt = $this->db->prepare("SELECT userID FROM User WHERE userID=? AND password =?");
        $stmt->bind_param('ss', $userID, $password);
        return $this->retrieveResult($stmt);
    }

    public function getUserDataByLogin($email, $password)
    {
        $stmt = $this->db->prepare("SELECT userID, role, name, email, phone, address, creditCard 
                                    FROM User
                                    WHERE email=? AND password=?");
        $stmt->bind_param('ss', $email, $password);
        $userData = $this->retrieveResult($stmt);
        return $this->getVendorData($userData);
    }

    public function getUserByID($userID)
    {
        $stmt = $this->db->prepare("SELECT userID, role, name, email, phone, address, creditCard FROM User WHERE userID=?");
        $stmt->bind_param('s', $userID);
        $userData = $this->retrieveResult($stmt);
        return $this->getVendorData($userData);
    }
    public function getUserByEmail($userEmail)
    {
        $stmt = $this->db->prepare("SELECT userID, role, name, email, phone, address, creditCard FROM User WHERE email=?");
        $stmt->bind_param('s', $userEmail);
        $userData = $this->retrieveResult($stmt);
        return $this->getVendorData($userData);
    }

    public function getUserReviewOfProduct($productID, $userID)
    {
        $stmt = $this->db->prepare("SELECT rating FROM Rating WHERE productID=? AND customerID =?");
        $stmt->bind_param('ss', $productID, $userID);
        return $this->retrieveResult($stmt);
    }

    public function getProductReviews($productID)
    {
        $stmt = $this->db->prepare("SELECT u.name, productID, customerID, rating, description FROM Rating r, User u WHERE r.productID=? AND r.customerID=u.userID");
        $stmt->bind_param("s", $productID);
        return $this->retrieveResult($stmt);
    }

    public function userIDFound($userID)
    {
        return !(empty($this->getUserByID($userID)));
    }
    public function productIDFound($productID)
    {
        return !(empty($this->getProducts(array($productID), false, false)));
    }
    public function getPurchase($userID, $productID)
    {
        $stmt = $this->db->prepare("SELECT productID, customerID, quantity
                                    FROM Purchase
                                    WHERE productID=? AND customerID=?");
        $stmt->bind_param('ss', $productID, $userID);
        return $this->retrieveResult($stmt);
    }

    public function getUserNotifications($userID)
    {
        $stmt = $this->db->prepare("SELECT notificationID, subject, content, userID
                                    FROM Notification
                                    WHERE userID = ?
                                    ORDER BY notificationID desc");
        $stmt->bind_param("s", $userID);
        return $this->retrieveResult($stmt);
    }

    public function insertNotification($userID, $subject, $content)
    {
        if ($this->userIDFound($userID)) {
            $stmt = $this->db->prepare("INSERT INTO Notification (notificationID, subject, content, userID) VALUES (?,?,?,?)");
            $notificationID = $this->getNextID('Notification', 'notificationID');
            $stmt->bind_param("ssss", $notificationID, $subject, $content, $userID);
            $stmt->execute();
            return $stmt->insert_id;
        }
        return false;
    }
    public function removeNotification($notificationID)
    {
        $stmt = $this->db->prepare("DELETE FROM Notification WHERE notificationID = ?");
        $stmt->bind_param("s", $notificationID);
        $stmt->execute();
        return $this->db->affected_rows >= 0;
    }
    public function insertPurchase($userID, $productID)
    {
        if ($this->userIDFound($userID) && $this->productIDFound($productID)) {
            $purchase = $this->getPurchase($userID, $productID);
            if (!isset($purchase[0])) {
                $stmt1 = $this->db->prepare("INSERT INTO Purchase (productID, customerID, quantity) VALUES (?,?,1)");
                $stmt1->bind_param("ss", $productID, $userID);
                $stmt1->execute();
                return $stmt1->insert_id;
            } else {
                $stmt2 = $this->db->prepare("UPDATE Purchase 
                                            SET quantity = ? 
                                            WHERE customerID = ? AND productID = ?");
                $updatedQuantity = $purchase[0]['quantity'] + 1;
                $stmt2->bind_param("iss", $updatedQuantity, $userID, $productID);
                $stmt2->execute();
                return $this->db->affected_rows >= 0;
            }
        }
        return false;
    }

    private function getVendorData($userData)
    {
        if (isset($userData[0])) {
            if ($userData[0]['role'] == DatabaseHelper::VENDOR_ROLE) {
                $stmt = $this->db->prepare("SELECT c.companyID, IVA, companyPEC
                FROM Agent a, Company c
                WHERE a.companyID=c.companyID AND a.userID = ?");
                $stmt->bind_param('s', $userData[0]['userID']);
                $result = $this->retrieveResult($stmt);
                $userData[0] = array_merge($userData[0], $result[0]);
            }
            return $userData[0];
        } else {
            return $userData;
        }
    }

    private function prepareInClauseVariableText($values)
    {
        return str_repeat('?,', count($values) - 1) . '?';
    }
    private function getNextID($tableName, $idName)
    {
        $this->requireValidTableName($tableName);
        $this->requireValidIDName($idName);
        $stmt = $this->db->prepare("SELECT $idName FROM $tableName ORDER BY $idName DESC LIMIT 1");
        $result = $this->retrieveResult($stmt);
        if (empty($result)) {
            return '0000000000000001';
        } else {
            $id = strval(intval($result[0][$idName]) + 1);
            return str_repeat('0', 16 - strlen($id)) . $id;
        }
    }
    private function requireValidTableName($tableName)
    {
        if (!in_array($tableName, DatabaseHelper::ENTITIES)) {
            die("DatabaseHelper.requireValidTableName: Invalid table name.");
        }
    }
    private function requireValidIDName($idName)
    {
        if (!in_array($idName, DatabaseHelper::IDS)) {
            die("DatabaseHelper.requireValidIDName: Invalid id name.");
        }
    }
    private function retrieveResult($stmt)
    {
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }
    private function getResultExcludingCart($result)
    {
        if (CartHelper::isCartEmpty()) {
            return $result;
        } else {
            $productsNotInCart = array();
            $productsInCart = CartHelper::getCart();
            foreach ($result as $productData) {
                if (!in_array($productData['productID'], $productsInCart)) {
                    array_push($productsNotInCart, $productData);
                }
            }
            return $productsNotInCart;
        }
    }
    private function getResultExcludingOwn($result)
    {
        if (!UserHelper::isLoggedIn() || UserHelper::isAdmin()) {
            return $result;
        } else {
            $ownedProducts = $this->getVendorProducts(UserHelper::getUserID());
            $notOwnedProducts = array();
            foreach ($result as $productData) {
                $owned = false;
                foreach ($ownedProducts as $ownedProduct) {
                    if ($productData['productID'] == $ownedProduct['productID']) {
                        $owned = true;
                    }
                }
                if (!$owned) {
                    array_push($notOwnedProducts, $productData);
                }
            }
            return $notOwnedProducts;
        }
    }
    /** DEBUG -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
    public function dbg_get_random_unavailable_products($n)
    {
        $stmt = $this->db->prepare("SELECT productID, name, image, shortDescription, price FROM Product WHERE availability=false ORDER BY RAND() LIMIT ?");
        $stmt->bind_param('i', $n);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }
    public function dbg_get_admin_notification()
    {
        $stmt = $this->db->prepare("SELECT notificationID, subject, content FROM Notification WHERE userID='0000000000000001' LIMIT 50");
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }
    /** -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
}
