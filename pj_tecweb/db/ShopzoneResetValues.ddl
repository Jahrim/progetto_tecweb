delete from Rating;
delete from Purchase;
delete from Agent;
delete from Company;
delete from Notification;
delete from Product;
delete from User;

insert into User(userID, role, name, email, password, phone, address, creditCard)
values ('0000000000000001', 'admin', 'admin', 'admin@shopzone.com', 'admin', NULL, NULL, NULL),
	   ('0000000000000002', 'vendor', 'vendor1', 'vendor1@shopzone.com', 'vendor1', 3791111111, 'Via delle vendite n1', '1111111111111111'),
	   ('0000000000000003', 'vendor', 'vendor2', 'vendor2@shopzone.com', 'vendor2', 3792222222, 'Via delle vendite n2', NULL),
	   ('0000000000000004', 'vendor', 'vendor3', 'vendor3@shopzone.com', 'vendor3', 3793333333, NULL, NULL),
	   ('0000000000000005', 'vendor', 'vendor4', 'vendor4@shopzone.com', 'vendor4', NULL, NULL, NULL),
	   ('0000000000000006', 'vendor', 'vendor5', 'vendor5@shopzone.com', 'vendor5', NULL, 'Via delle vendite n5', '5555555555555555'),
	   ('0000000000000007', 'vendor', 'vendor6', 'vendor6@shopzone.com', 'vendor6', NULL, NULL, '6666666666666666'),
	   ('0000000000000008', 'vendor', 'vendor7', 'vendor7@shopzone.com', 'vendor7', NULL, 'Via delle vendite n7', NULL),
	   ('0000000000000009', 'vendor', 'vendor8', 'vendor8@shopzone.com', 'vendor8', 3798888888, NULL, '8888888888888888'),
	   ('0000000000000010', 'vendor', 'vendor9', 'vendor9@shopzone.com', 'vendor9', NULL, 'Via delle vendite n9', '8888888888888888'),
	   ('0000000000000011', 'vendor', 'vendor10', 'vendor10@shopzone.com', 'vendor10', 3791010101, 'Via delle vendite n10', '1010101010101010'),
	   ('0000000000000012', 'user', 'user1', 'user1@shopzone.com', 'user1', 3721111111, 'Via degli acquisti n1', '1111111111111110'),
	   ('0000000000000013', 'user', 'user2', 'user2@shopzone.com', 'user2', 3722222222, 'Via degli acquisti n2', NULL),
	   ('0000000000000014', 'user', 'user3', 'user3@shopzone.com', 'user3', 3723333333, NULL, NULL),
	   ('0000000000000015', 'user', 'user4', 'user4@shopzone.com', 'user4', NULL, NULL, NULL),
	   ('0000000000000016', 'user', 'user5', 'user5@shopzone.com', 'user5', NULL, 'Via degli acquisti n5', '5555555555555550'),
	   ('0000000000000017', 'user', 'user6', 'user6@shopzone.com', 'user6', NULL, NULL, '6666666666666660'),
	   ('0000000000000018', 'user', 'user7', 'user7@shopzone.com', 'user7', NULL, 'Via degli acquisti n7', NULL);
	   
insert into Company(companyID, IVA, companyPEC)
values ('0000000000000001','12341234123','melashopping@megamail.com'),
	   ('0000000000000002','12341234124','fruttaeverdura@megamail.com'),
	   ('0000000000000003','12341234125','elettrAR@megamail.com'),
	   ('0000000000000004','12341234126','cucinecesari@megamail.com'),
	   ('0000000000000005','12341234127','stoppalt@megamail.com'),
	   ('0000000000000006','12341234128','phonyshopping@megamail.com'),
	   ('0000000000000007','12341234129','cygolf@megamail.com'),
	   ('0000000000000008','12341234133','KERNandco@megamail.com'),
	   ('0000000000000009','12341234143','puffpuffcosmetica@megamail.com'),
	   ('0000000000000010','12341234153','bigmacchine@megamail.com');
	   
insert into Agent(companyID, userID)
values ('0000000000000001','0000000000000002'),
	   ('0000000000000002','0000000000000003'),
       ('0000000000000003','0000000000000004'),
       ('0000000000000004','0000000000000005'),
       ('0000000000000005','0000000000000006'),
       ('0000000000000006','0000000000000007'),
       ('0000000000000007','0000000000000008'),
       ('0000000000000008','0000000000000009'),
       ('0000000000000009','0000000000000010'),
       ('0000000000000010','0000000000000011');

insert into Product(productID, name, image, shortDescription, longDescription, price, availability, averageRating, vendorID)
values ('0000000000000001', 'EmptyProduct1', 'product-img.png', 'Empty Product 1', 'This is a debug empty item.', 0, false, 0, '0000000000000001'),
	   ('0000000000000002', 'EmptyProduct2', 'product-img.png', 'Empty Product 2', 'This is a debug empty item.', 0, false, 0, '0000000000000001'),
	   ('0000000000000003', 'EmptyProduct3', 'product-img.png', 'Empty Product 3', 'This is a debug empty item.', 0, false, 0, '0000000000000001'),
	   ('0000000000000004', 'EmptyProduct4', 'product-img.png', 'Empty Product 4', 'This is a debug empty item.', 0, false, 0, '0000000000000001'),
	   ('0000000000000005', 'EmptyProduct5', 'product-img.png', 'Empty Product 5', 'This is a debug empty item.', 0, false, 0, '0000000000000001'),
	   ('0000000000000006', 'Mela Telefono 1V1', 'mela-telefono.png', 'Telefono con il simbolo della mela.', 'Telefono con il simbolo della mela venduto da vendor1.', 80, true, 0, '0000000000000002'),
	   ('0000000000000007', 'Mela Telefono 2V1', 'mela-telefono.png', 'Telefono con il simbolo della mela.', 'Telefono con il simbolo della mela venduto da vendor1.', 120, true, 0, '0000000000000002'),
	   ('0000000000000008', 'Mela Telefono 3V1', 'mela-telefono.png', 'Telefono con il simbolo della mela.', 'Telefono con il simbolo della mela venduto da vendor1.', 140, true, 0, '0000000000000002'),
	   ('0000000000000009', 'Mela Telefono 4V1', 'mela-telefono.png', 'Telefono con il simbolo della mela.', 'Telefono con il simbolo della mela venduto da vendor1.', 200, true, 0, '0000000000000002'),
	   ('0000000000000010', 'Mela Calcolatore 1V1', 'mela-computer.png', 'Calcolatore con il simbolo della mela.', 'Calcolatore con il simbolo della mela venduto da vendor1.', 180, true, 0, '0000000000000002'),
	   ('0000000000000011', 'Mela Calcolatore 2V1', 'mela-computer.png', 'Calcolatore con il simbolo della mela.', 'Calcolatore con il simbolo della mela venduto da vendor1.', 280, true, 0, '0000000000000002'),
	   ('0000000000000012', 'Mela Calcolatore 3V1', 'mela-computer.png', 'Calcolatore con il simbolo della mela.', 'Calcolatore con il simbolo della mela venduto da vendor1.', 360, true, 0, '0000000000000002'),
	   ('0000000000000013', 'Mela Calcolatore 4V1', 'mela-computer.png', 'Calcolatore con il simbolo della mela.', 'Calcolatore con il simbolo della mela venduto da vendor1.', 480, true, 0, '0000000000000002'),
	   ('0000000000000014', 'Mela Calcolatore 5V1', 'mela-computer.png', 'Calcolatore con il simbolo della mela.', 'Calcolatore con il simbolo della mela venduto da vendor1.', 660, true, 0, '0000000000000002'),
	   ('0000000000000015', 'Mela Calcolatore 6V1', 'mela-computer.png', 'Calcolatore con il simbolo della mela.', 'Calcolatore con il simbolo della mela venduto da vendor1.', 1200, true, 0, '0000000000000002'),
	   ('0000000000000016', 'Tazza KERN 1V8', 'kern-cup.png', 'Tazza del famoso istituto scientifico KERN.', 'Tazza del famoso istituto scientifico KERN venduta da vendor8.', 10, true, 0, '0000000000000009'),
	   ('0000000000000017', 'Tazza KERN 2V8', 'kern-cup.png', 'Tazza del famoso istituto scientifico KERN.', 'Tazza del famoso istituto scientifico KERN venduta da vendor8.', 15, true, 0, '0000000000000009'),
	   ('0000000000000018', 'Tazza KERN 3V8', 'kern-cup.png', 'Tazza del famoso istituto scientifico KERN.', 'Tazza del famoso istituto scientifico KERN venduta da vendor8.', 20, true, 0, '0000000000000009'),
	   ('0000000000000019', 'Tazza KERN 4V8', 'kern-cup.png', 'Tazza del famoso istituto scientifico KERN.', 'Tazza del famoso istituto scientifico KERN venduta da vendor8.', 12, true, 0, '0000000000000009'),
	   ('0000000000000020', 'Tazza KERN 5V8', 'kern-cup.png', 'Tazza del famoso istituto scientifico KERN.', 'Tazza del famoso istituto scientifico KERN venduta da vendor8.', 22, true, 0, '0000000000000009'),
	   ('0000000000000021', 'Tazza KERN 6V8', 'kern-cup.png', 'Tazza del famoso istituto scientifico KERN.', 'Tazza del famoso istituto scientifico KERN venduta da vendor8.', 16, true, 0, '0000000000000009'),
	   ('0000000000000022', 'Tazza KERN 7V8', 'kern-cup.png', 'Tazza del famoso istituto scientifico KERN.', 'Tazza del famoso istituto scientifico KERN venduta da vendor8.', 35, true, 0, '0000000000000009'),
	   ('0000000000000023', 'Maglia della NAZA 1V8', 'naza-shirt.png', 'Maglia del famoso istituto scientifico NAZA.', 'Maglia del famoso istituto scientifico NAZA venduta da vendor8.', 30, true, 0, '0000000000000009'),
	   ('0000000000000024', 'Maglia della NAZA 2V8', 'naza-shirt.png', 'Maglia del famoso istituto scientifico NAZA.', 'Maglia del famoso istituto scientifico NAZA venduta da vendor8.', 34, true, 0, '0000000000000009'),
	   ('0000000000000025', 'Maglia della NAZA 3V8', 'naza-shirt.png', 'Maglia del famoso istituto scientifico NAZA.', 'Maglia del famoso istituto scientifico NAZA venduta da vendor8.', 38, true, 0, '0000000000000009'),
	   ('0000000000000026', 'Maglia della NAZA 4V8', 'naza-shirt.png', 'Maglia del famoso istituto scientifico NAZA.', 'Maglia del famoso istituto scientifico NAZA venduta da vendor8.', 42, true, 0, '0000000000000009'),
	   ('0000000000000027', 'Modellino della FerrarO 1V10', 'FerrarO.png', 'Modellino della macchina FerrarO.', 'Modellino della macchina FerrarO in scala 1:10 venduta da vendor10.', 90, true, 0, '0000000000000011'),
	   ('0000000000000028', 'Modellino della FerrarO 2V10', 'FerrarO.png', 'Modellino della macchina FerrarO.', 'Modellino della macchina FerrarO in scala 1:10 venduta da vendor10.', 190, true, 0, '0000000000000011'),
	   ('0000000000000029', 'Modellino della FerrarO 3V10', 'FerrarO.png', 'Modellino della macchina FerrarO.', 'Modellino della macchina FerrarO in scala 1:10 venduta da vendor10.', 350, true, 0, '0000000000000011'),
	   ('0000000000000030', 'Modellino della FerrarO 4V10', 'FerrarO.png', 'Modellino della macchina FerrarO.', 'Modellino della macchina FerrarO in scala 1:10 venduta da vendor10.', 470, true, 0, '0000000000000011'),
	   ('0000000000000031', 'Modellino della FerrarO 5V10', 'FerrarO.png', 'Modellino della macchina FerrarO.', 'Modellino della macchina FerrarO in scala 1:10 venduta da vendor10.', 20, true, 0, '0000000000000011'),
	   ('0000000000000032', 'Modellino della FerrarO 6V10', 'FerrarO.png', 'Modellino della macchina FerrarO.', 'Modellino della macchina FerrarO in scala 1:10 venduta da vendor10.', 900, true, 3, '0000000000000011'),
	   ('0000000000000033', 'Modellino della MaseratO 1V10', 'MaseratO.png', 'Modellino della macchina MaseratO.', 'Modellino della macchina MaseratO in scala 1:8 venduta da vendor10.', 250, true, 0, '0000000000000011'),
	   ('0000000000000034', 'Modellino della MaseratO 2V10', 'MaseratO.png', 'Modellino della macchina MaseratO.', 'Modellino della macchina MaseratO in scala 1:8 venduta da vendor10.', 140, true, 0, '0000000000000011'),
	   ('0000000000000035', 'Modellino della MaseratO 3V10', 'MaseratO.png', 'Modellino della macchina MaseratO.', 'Modellino della macchina MaseratO in scala 1:8 venduta da vendor10.', 90, true, 0, '0000000000000011'),
	   ('0000000000000036', 'Modellino della MaseratO 4V10', 'MaseratO.png', 'Modellino della macchina MaseratO.', 'Modellino della macchina MaseratO in scala 1:8 venduta da vendor10.', 320, true, 0, '0000000000000011');
	   
insert into Purchase(productID, customerID, quantity)
values ('0000000000000006', '0000000000000012', 4),
	   ('0000000000000010', '0000000000000012', 3),
	   ('0000000000000025', '0000000000000012', 2),
	   ('0000000000000017', '0000000000000012', 4),
	   ('0000000000000033', '0000000000000012', 1),
	   ('0000000000000033', '0000000000000013', 1),
	   ('0000000000000033', '0000000000000014', 1),
	   ('0000000000000033', '0000000000000015', 1);
	   
insert into Rating(productID,customerID,rating,description)
values ('0000000000000033', '0000000000000012', 4, 'Modellino piuttosto grande!'),
	   ('0000000000000033', '0000000000000013', 3, 'Prodotto discreto, nella media...'),
	   ('0000000000000033', '0000000000000014', 1, 'Pensavo fosse una macchina vera. Contrariato.'),
	   ('0000000000000033', '0000000000000015', 5, 'Ottimo prodotto per il prezzo di vendita!');
	   
insert into Notification(notificationID,subject,content,userID)
values ('0000000000000001', 'Benvenuto!', 'Grazie per aver scelto ShopZone!', '0000000000000002'),
       ('0000000000000002', 'Benvenuto!', 'Grazie per aver scelto ShopZone!', '0000000000000003'),
	   ('0000000000000003', 'Benvenuto!', 'Grazie per aver scelto ShopZone!', '0000000000000004'),
	   ('0000000000000004', 'Benvenuto!', 'Grazie per aver scelto ShopZone!', '0000000000000005'),
	   ('0000000000000005', 'Benvenuto!', 'Grazie per aver scelto ShopZone!', '0000000000000006'),
	   ('0000000000000006', 'Benvenuto!', 'Grazie per aver scelto ShopZone!', '0000000000000007'),
	   ('0000000000000007', 'Benvenuto!', 'Grazie per aver scelto ShopZone!', '0000000000000008'),
	   ('0000000000000008', 'Benvenuto!', 'Grazie per aver scelto ShopZone!', '0000000000000009'),
	   ('0000000000000009', 'Benvenuto!', 'Grazie per aver scelto ShopZone!', '0000000000000010'),
	   ('0000000000000010', 'Benvenuto!', 'Grazie per aver scelto ShopZone!', '0000000000000011'),
	   ('0000000000000011', 'Benvenuto!', 'Grazie per aver scelto ShopZone!', '0000000000000012'),
	   ('0000000000000012', 'Benvenuto!', 'Grazie per aver scelto ShopZone!', '0000000000000013'),
	   ('0000000000000013', 'Benvenuto!', 'Grazie per aver scelto ShopZone!', '0000000000000014'),
	   ('0000000000000014', 'Benvenuto!', 'Grazie per aver scelto ShopZone!', '0000000000000015'),
	   ('0000000000000015', 'Benvenuto!', 'Grazie per aver scelto ShopZone!', '0000000000000016'),
	   ('0000000000000016', 'Benvenuto!', 'Grazie per aver scelto ShopZone!', '0000000000000017'),
	   ('0000000000000017', 'Benvenuto!', 'Grazie per aver scelto ShopZone!', '0000000000000018');
	   

