-- *********************************************
-- * SQL MySQL generation                      
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.1              
-- * Generator date: Dec  4 2018              
-- * Generation date: Thu Jan 21 17:09:05 2021 
-- * LUN file:  
-- * Schema: Shopzone/1 
-- ********************************************* 


-- Database Section
-- ________________ 

create database Shopzone;
use Shopzone;


-- Tables Section
-- _____________ 

create table User (
     userID char(16) not null,
     role varchar(16) not null,
     name varchar(32) not null,
     email varchar(320) not null,
     password varchar(32) not null,
     phone varchar(15),
     address varchar(64),
     creditCard char(16),
     constraint IDUSER primary key (userID));

create table Product (
     productID char(16) not null,
     name varchar(32) not null,
     image varchar(255) not null,
     shortDescription varchar(255) not null,
     longDescription varchar(2048) not null,
     price int not null,
     availability char not null,
     averageRating int,
     vendorID char(16) not null,
     constraint IDPRODUCT primary key (productID));

create table Notification (
     notificationID char(16) not null,
     subject varchar(32) not null,
     content varchar(255) not null,
     userID char(16) not null,
     constraint IDNotification primary key (notificationID));

create table Company (
     companyID char(16) not null,
     IVA char(11) not null,
     companyPEC varchar(320) not null,
     constraint IDCompany_ID primary key (companyID));

create table Agent (
     companyID char(16) not null,
     userID char(16) not null,
     constraint IDAgent primary key (companyID, userID));

create table Purchase (
     productID char(16) not null,
     customerID char(16) not null,
     quantity int not null,
     constraint IDPurchase primary key (productID, customerID));

create table Rating (
     productID char(16) not null,
     customerID char(16) not null,
     rating int not null,
     description varchar(255) not null,
     constraint IDRating primary key (productID, customerID));


-- Constraints Section
-- ___________________ 

alter table Product add constraint FKSale
     foreign key (vendorID)
     references User (userID);

alter table Notification add constraint FKAlert
     foreign key (userID)
     references User (userID);

-- Not implemented
-- alter table Company add constraint IDCompany_CHK
--     check(exists(select * from Agent
--                  where Agent.companyID = companyID)); 

alter table Agent add constraint FKAge_Use
     foreign key (userID)
     references User (userID);

alter table Agent add constraint FKAge_Com
     foreign key (companyID)
     references Company (companyID);

alter table Purchase add constraint FKPur_Use
     foreign key (customerID)
     references User (userID);

alter table Purchase add constraint FKPur_Pro
     foreign key (productID)
     references Product (productID);

alter table Rating add constraint FKRat_Use
     foreign key (customerID)
     references User (userID);

alter table Rating add constraint FKRat_Pro
     foreign key (productID)
     references Product (productID);


-- Index Section
-- _____________ 

